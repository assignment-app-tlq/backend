package com.backend.assignmentapp.dtos.requests;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;

@Getter
@Setter
public class AssignmentRequest {
    private Long id;
    private String name;
    private Long divisionId;
    private MultipartFile questionFile;
    @JsonFormat(pattern="dd-MM-yyyy")
    private Date submitDate;
    @JsonFormat(pattern="dd-MM-yyyy")
    private Date createDate;
}
