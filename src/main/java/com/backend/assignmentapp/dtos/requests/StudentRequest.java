package com.backend.assignmentapp.dtos.requests;

import com.backend.assignmentapp.entities.User;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class StudentRequest {
    private Long id;
    private User user;
    private Long classId;
}
