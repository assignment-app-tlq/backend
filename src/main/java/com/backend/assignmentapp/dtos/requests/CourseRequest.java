package com.backend.assignmentapp.dtos.requests;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CourseRequest {
    private Long id;
    private String name;
}
