package com.backend.assignmentapp.dtos.requests;

import com.backend.assignmentapp.entities.User;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TeacherRequest {
    private Long id;
    private User user;
}
