package com.backend.assignmentapp.dtos.requests;

import com.backend.assignmentapp.entities.Course;
import com.backend.assignmentapp.entities.Major;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClassRequest {
    private Long id;
    private String name;
    private Long courseId;
    private Long majorId;
}
