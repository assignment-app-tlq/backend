package com.backend.assignmentapp.dtos.requests;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class StudentUserRequest {
    private String firstName;
    private String lastName;
    private byte gender;
    @JsonFormat(pattern="dd-MM-yyyy")
    private Date dateOfBirth;
    private String email;
    private String password;
    private Long classId;
}
