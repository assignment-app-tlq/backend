package com.backend.assignmentapp.dtos.requests;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DivisionRequest {
    private Long id;
    private Long subjectId;
    private Long classId;
    private Long teacherId;
}
