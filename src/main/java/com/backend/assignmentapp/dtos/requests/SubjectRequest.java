package com.backend.assignmentapp.dtos.requests;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SubjectRequest {
    private Long id;
    private String name;
    private int duration;
}
