package com.backend.assignmentapp.dtos.responses;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StudentResponse {
    private Long id;
    private UserResponse user;
    private ClassResponse classInfo;
}
