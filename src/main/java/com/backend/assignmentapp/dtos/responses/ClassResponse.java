package com.backend.assignmentapp.dtos.responses;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClassResponse {
    private Long id;
    private String name;
    private CourseResponse course;
    private MajorResponse major;
}
