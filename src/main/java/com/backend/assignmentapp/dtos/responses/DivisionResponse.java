package com.backend.assignmentapp.dtos.responses;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DivisionResponse {
    private Long id;
    private SubjectResponse subject;
    private ClassResponse classInfo;
    private TeacherResponse teacher;
}
