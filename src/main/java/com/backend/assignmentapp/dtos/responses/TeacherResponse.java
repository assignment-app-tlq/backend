package com.backend.assignmentapp.dtos.responses;

import com.backend.assignmentapp.entities.User;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TeacherResponse {
    private Long id;
    private UserResponse user;
}
