package com.backend.assignmentapp.dtos.responses;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CourseResponse {
    private Long id;
    private String name;
}
