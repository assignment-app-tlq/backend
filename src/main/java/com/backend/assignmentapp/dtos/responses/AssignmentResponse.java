package com.backend.assignmentapp.dtos.responses;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public class AssignmentResponse {
    private Long id;
    private String name;
    private DivisionResponse division;
    private String questionFile;
    @JsonFormat(pattern="dd-MM-yyyy")
    private Date submitDate;
    @JsonFormat(pattern="dd-MM-yyyy")
    private Date createDate;
}
