package com.backend.assignmentapp.dtos.responses;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MajorResponse {
    private Long id;
    private String name;
}
