package com.backend.assignmentapp.modelmappers;

import com.backend.assignmentapp.daos.CourseDao;
import com.backend.assignmentapp.daos.MajorDao;
import com.backend.assignmentapp.dtos.requests.ClassRequest;
import com.backend.assignmentapp.dtos.responses.ClassResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.backend.assignmentapp.entities.Class;

@Component
public class ClassMapper {
    private final CourseMapper courseMapper;
    private final MajorMapper majorMapper;
    private final CourseDao courseDao;
    private final MajorDao majorDao;

    @Autowired
    public ClassMapper(CourseMapper courseMapper, MajorMapper majorMapper, com.backend.assignmentapp.daos.CourseDao courseDao, MajorDao majorDao) {
        this.courseMapper = courseMapper;
        this.majorMapper = majorMapper;
        this.courseDao = courseDao;
        this.majorDao = majorDao;
    }

    public Class convertToEntity(ClassRequest classRequest) {
        Class classes = new Class();
        classes.setId(classRequest.getId());
        classes.setName(classRequest.getName());
        classes.setCourse(courseDao.getById(classRequest.getCourseId()));
        classes.setMajor(majorDao.getById(classRequest.getMajorId()));
        return classes;
    }

    public ClassResponse convertToResponse(Class classes) {
        ClassResponse classResponse = new ClassResponse();
        classResponse.setId(classes.getId());
        classResponse.setName(classes.getName());
        classResponse.setCourse(courseMapper.convertToResponse(classes.getCourse()));
        classResponse.setMajor(majorMapper.convertToResponse(classes.getMajor()));
        return classResponse;
    }
}
