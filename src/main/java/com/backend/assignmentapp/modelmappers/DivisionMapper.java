package com.backend.assignmentapp.modelmappers;

import com.backend.assignmentapp.daos.ClassDao;
import com.backend.assignmentapp.daos.SubjectDao;
import com.backend.assignmentapp.daos.TeacherDao;
import com.backend.assignmentapp.dtos.requests.DivisionRequest;
import com.backend.assignmentapp.dtos.responses.DivisionResponse;
import com.backend.assignmentapp.entities.Division;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DivisionMapper {
    private final ClassDao classDao;
    private final SubjectDao subjectDao;
    private final TeacherDao teacherDao;
    private final ClassMapper classMapper;
    private final TeacherMapper teacherMapper;
    private final SubjectMapper subjectMapper;

    @Autowired
    public DivisionMapper(ClassDao classDao, SubjectDao subjectDao, TeacherDao teacherDao, ClassMapper classMapper, TeacherMapper teacherMapper, SubjectMapper subjectMapper) {
        this.classDao = classDao;
        this.subjectDao = subjectDao;
        this.teacherDao = teacherDao;
        this.classMapper = classMapper;
        this.teacherMapper = teacherMapper;
        this.subjectMapper = subjectMapper;
    }

    public Division convertToEntity(DivisionRequest divisionRequest) {
        Division division = new Division();
        division.setId(divisionRequest.getId());
        division.setClasses(classDao.getById(divisionRequest.getClassId()));
        division.setSubject(subjectDao.getById(divisionRequest.getSubjectId()));
        division.setTeacher(teacherDao.getById(divisionRequest.getTeacherId()));
        return division;
    }

    public DivisionResponse convertToResponse(Division division) {
        DivisionResponse divisionResponse = new DivisionResponse();
        divisionResponse.setId(division.getId());
        divisionResponse.setClassInfo(classMapper.convertToResponse(division.getClasses()));
        divisionResponse.setSubject(subjectMapper.convertToResponse(division.getSubject()));
        divisionResponse.setTeacher(teacherMapper.convertToResponse(division.getTeacher()));
        return divisionResponse;
    }
}
