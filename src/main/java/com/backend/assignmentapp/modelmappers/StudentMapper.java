package com.backend.assignmentapp.modelmappers;

import com.backend.assignmentapp.daos.ClassDao;
import com.backend.assignmentapp.dtos.requests.StudentRequest;
import com.backend.assignmentapp.dtos.responses.StudentResponse;
import com.backend.assignmentapp.entities.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StudentMapper {
    private final UserMapper userMapper;
    private final ClassMapper classMapper;
    private final ClassDao classDao;

    @Autowired
    public StudentMapper(UserMapper userMapper, ClassMapper classMapper, ClassDao classDao) {
        this.userMapper = userMapper;
        this.classMapper = classMapper;
        this.classDao = classDao;
    }

    public Student convertToEntity(StudentRequest studentRequest) {
        Student student = new Student();
        student.setId(studentRequest.getId());
        student.setUser(studentRequest.getUser());
        student.setClasses(classDao.getById(studentRequest.getClassId()));
        return student;
    }

    public StudentResponse convertToResponse(Student student) {
        StudentResponse studentResponse = new StudentResponse();
        studentResponse.setId(student.getId());
        studentResponse.setUser(userMapper.convertToResponse(student.getUser()));
        studentResponse.setClassInfo(classMapper.convertToResponse(student.getClasses()));
        return studentResponse;
    }
}
