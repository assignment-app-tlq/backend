package com.backend.assignmentapp.modelmappers;

import com.backend.assignmentapp.daos.DivisionDao;
import com.backend.assignmentapp.dtos.requests.AssignmentRequest;
import com.backend.assignmentapp.dtos.responses.AssignmentResponse;
import com.backend.assignmentapp.entities.Assignment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AssignmentMapper {
    private final DivisionDao divisionDao;
    private final DivisionMapper divisionMapper;

    @Autowired
    public AssignmentMapper(DivisionDao divisionDao, DivisionMapper divisionMapper) {
        this.divisionDao = divisionDao;
        this.divisionMapper = divisionMapper;
    }

    public Assignment convertToEntity(AssignmentRequest assignmentRequest) {
        Assignment assignment = new Assignment();
        assignment.setId(assignmentRequest.getId());
        assignment.setName(assignmentRequest.getName());
        assignment.setDivision(divisionDao.getById(assignmentRequest.getDivisionId()));
        assignment.setQuestion(assignmentRequest.getQuestionFile().getOriginalFilename());
        assignment.setSubmitDate(assignmentRequest.getSubmitDate());
        assignment.setCreatedDate(assignmentRequest.getCreateDate());
        return assignment;
    }

    public AssignmentResponse convertToResponse(Assignment assignment) {
        AssignmentResponse assignmentResponse = new AssignmentResponse();
        assignmentResponse.setId(assignment.getId());
        assignmentResponse.setName(assignment.getName());
        assignmentResponse.setDivision(divisionMapper.convertToResponse(assignment.getDivision()));
        assignmentResponse.setQuestionFile(assignment.getQuestion());
        assignmentResponse.setSubmitDate(assignment.getSubmitDate());
        assignmentResponse.setCreateDate(assignment.getCreatedDate());
        return assignmentResponse;
    }
}
