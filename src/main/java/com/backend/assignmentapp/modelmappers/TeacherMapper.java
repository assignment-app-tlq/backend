package com.backend.assignmentapp.modelmappers;

import com.backend.assignmentapp.daos.UserDao;
import com.backend.assignmentapp.dtos.requests.TeacherRequest;
import com.backend.assignmentapp.dtos.requests.TeacherUserRequest;
import com.backend.assignmentapp.dtos.requests.UserRequest;
import com.backend.assignmentapp.dtos.responses.TeacherResponse;
import com.backend.assignmentapp.entities.Teacher;
import com.backend.assignmentapp.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TeacherMapper {
    private final UserDao userDao;
    private final UserMapper userMapper;

    @Autowired
    public TeacherMapper(UserDao userDao, UserMapper userMapper) {
        this.userDao = userDao;
        this.userMapper = userMapper;
    }

    public Teacher convertToEntity(TeacherRequest teacherRequest) {
        Teacher teacher = new Teacher();
        teacher.setId(teacherRequest.getId());
        teacher.setUser(teacherRequest.getUser());
        return teacher;
    }

    public TeacherResponse convertToResponse(Teacher teacher) {
        TeacherResponse teacherResponse = new TeacherResponse();
        teacherResponse.setId(teacher.getId());
        teacherResponse.setUser(userMapper.convertToResponse(teacher.getUser()));
        return teacherResponse;
    }
}
