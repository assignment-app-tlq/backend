package com.backend.assignmentapp.modelmappers;

import com.backend.assignmentapp.dtos.requests.SubjectRequest;
import com.backend.assignmentapp.dtos.responses.SubjectResponse;
import com.backend.assignmentapp.entities.Subject;
import org.springframework.stereotype.Component;

@Component
public class SubjectMapper {
    public Subject convertToEntity(SubjectRequest subjectRequest) {
        Subject subject = new Subject();
        subject.setId(subjectRequest.getId());
        subject.setName(subjectRequest.getName());
        subject.setDuration(subjectRequest.getDuration());
        return subject;
    }

    public SubjectResponse convertToResponse(Subject subject) {
        SubjectResponse subjectResponse = new SubjectResponse();
        subjectResponse.setId(subject.getId());
        subjectResponse.setName(subject.getName());
        subjectResponse.setDuration(subject.getDuration());
        return subjectResponse;
    }
}
