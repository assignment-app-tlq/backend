package com.backend.assignmentapp.modelmappers;

import com.backend.assignmentapp.daos.RoleDao;
import com.backend.assignmentapp.dtos.requests.StudentUserRequest;
import com.backend.assignmentapp.dtos.requests.TeacherUserRequest;
import com.backend.assignmentapp.dtos.requests.UserRequest;
import com.backend.assignmentapp.dtos.responses.UserResponse;
import com.backend.assignmentapp.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {
    public final RoleMapper roleMapper;
    public final RoleDao roleDao;

    @Autowired
    public UserMapper(RoleMapper roleMapper, RoleDao roleDao) {
        this.roleMapper = roleMapper;
        this.roleDao = roleDao;
    }

    public UserRequest convertToRequest(StudentUserRequest studentUserRequest) {
        UserRequest userRequest = new UserRequest();
        userRequest.setFirstName(studentUserRequest.getFirstName());
        userRequest.setEmail(studentUserRequest.getEmail());
        userRequest.setLastName(studentUserRequest.getLastName());
        userRequest.setGender(studentUserRequest.getGender());
        userRequest.setPassword(studentUserRequest.getPassword());
        userRequest.setDateOfBirth(studentUserRequest.getDateOfBirth());
        userRequest.setRoleId(3L);
        return userRequest;
    }

    public UserRequest convertToRequest(TeacherUserRequest teacherUserRequest) {
        UserRequest userRequest = new UserRequest();
        userRequest.setFirstName(teacherUserRequest.getFirstName());
        userRequest.setEmail(teacherUserRequest.getEmail());
        userRequest.setLastName(teacherUserRequest.getLastName());
        userRequest.setGender(teacherUserRequest.getGender());
        userRequest.setPassword(teacherUserRequest.getPassword());
        userRequest.setDateOfBirth(teacherUserRequest.getDateOfBirth());
        userRequest.setRoleId(2L);
        return userRequest;
    }

    public User convertToEntity(UserRequest userRequest) {
        User user = new User();
        user.setId(userRequest.getId());
        user.setFirstName(userRequest.getFirstName());
        user.setEmail(userRequest.getEmail());
        user.setLastName(userRequest.getLastName());
        user.setGender(userRequest.getGender());
        user.setPassword(userRequest.getPassword());
        user.setDateOfBirth(userRequest.getDateOfBirth());
        user.setRole(roleDao.getById(userRequest.getRoleId()));
        return user;
    }

    public UserResponse convertToResponse(User user) {
        UserResponse userResponse = new UserResponse();
        userResponse.setId(user.getId());
        userResponse.setFirstName(user.getFirstName());
        userResponse.setLastName(user.getLastName());
        userResponse.setGender(user.getGender());
        userResponse.setEmail(user.getEmail());
        userResponse.setPassword(user.getPassword());
        userResponse.setDateOfBirth(user.getDateOfBirth());
        userResponse.setRole(roleMapper.convertToResponse(user.getRole()));
        return userResponse;
    }
}
