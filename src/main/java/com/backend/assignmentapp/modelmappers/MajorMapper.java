package com.backend.assignmentapp.modelmappers;

import com.backend.assignmentapp.dtos.requests.MajorRequest;
import com.backend.assignmentapp.dtos.responses.MajorResponse;
import com.backend.assignmentapp.entities.Major;
import org.springframework.stereotype.Component;

@Component
public class MajorMapper {
    public Major convertToEntity(MajorRequest majorRequest) {
        Major major = new Major();
        major.setId(majorRequest.getId());
        major.setName(majorRequest.getName());
        return major;
    }

    public MajorResponse convertToResponse(Major major) {
        MajorResponse majorResponse = new MajorResponse();
        majorResponse.setId(major.getId());
        majorResponse.setName(major.getName());
        return majorResponse;
    }
}
