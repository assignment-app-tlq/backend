package com.backend.assignmentapp.modelmappers;

import com.backend.assignmentapp.dtos.requests.CourseRequest;
import com.backend.assignmentapp.dtos.responses.CourseResponse;
import com.backend.assignmentapp.entities.Course;
import org.springframework.stereotype.Component;

@Component
public class CourseMapper {
    public Course convertToEntity(CourseRequest courseRequest) {
        Course course = new Course();
        course.setId(courseRequest.getId());
        course.setName(courseRequest.getName());
        return course;
    }

    public CourseResponse convertToResponse(Course course) {
        CourseResponse courseResponse = new CourseResponse();
        courseResponse.setId(course.getId());
        courseResponse.setName(course.getName());
        return courseResponse;
    }
}
