package com.backend.assignmentapp.daos;

import com.backend.assignmentapp.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserDao extends JpaRepository<User, Long> {
    @Query("SELECT m from User m WHERE m.firstName LIKE %:name% or m.lastName LIKE %:name%")
    Page<User> findByName(@Param("name") String name, Pageable pageable);

    @Query(value = "SELECT * FROM \"user\" WHERE \"user\".email ILIKE :email", nativeQuery = true)
    Optional<User> findDuplicateEmailWithoutId(@Param("email") String email);

    @Query(value = "SELECT * FROM \"user\" WHERE \"user\".email ILIKE :email AND id <> :id", nativeQuery = true)
    Optional<User> findDuplicateEmailWithId(@Param("id") Long id, @Param("email") String email);
}
