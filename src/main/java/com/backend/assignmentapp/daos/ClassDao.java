package com.backend.assignmentapp.daos;

import com.backend.assignmentapp.entities.Class;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
public interface ClassDao extends JpaRepository<Class, Long> {

//    @Query("SELECT m FROM Class m inner join Major n on m.major_id = n.id inner join Course v on m.course_id = v.id where n.name LIKE :name OR m.name LIKE :name OR v.name LIKE :name")
    @Query(value = "SELECT * FROM class INNER JOIN course ON class.course_id = course.id INNER JOIN major ON class.major_id = major.id WHERE class.name LIKE %:name% OR major.name LIKE %:name% OR course.name LIKE %:name%", nativeQuery = true)
    Page<Class> findByName(@Param("name") String name, Pageable pageable);

    @Query(value = "SELECT * FROM class INNER JOIN course ON class.course_id = course.id INNER JOIN major ON class.major_id = major.id WHERE class.name ILIKE :name AND major.id = :majorId AND course.id = :courseId", nativeQuery = true)
    Optional<Class> findDuplicateClassWithoutId(@Param("name") String name, @Param("majorId") Long major_id,@Param("courseId") Long course_id);

    @Query(value = "SELECT * FROM class INNER JOIN course ON class.course_id = course.id INNER JOIN major ON class.major_id = major.id WHERE class.name ILIKE :name AND major.id = :majorId AND course.id = :courseId AND class.id <> :id", nativeQuery = true)
    Optional<Class> findDuplicateClassWithId(@Param("id") Long id, @Param("name") String name, @Param("majorId") Long major_id,@Param("courseId") Long course_id);
}
