package com.backend.assignmentapp.daos;

import com.backend.assignmentapp.entities.Role;
import com.backend.assignmentapp.exceptions.ResourceNotFoundException;
import org.springframework.data.jpa.repository.Modifying;
import com.backend.assignmentapp.dtos.requests.MajorRequest;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.backend.assignmentapp.entities.Major;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
public interface MajorDao extends JpaRepository<Major, Long> {
    @Query(value = "select * from major where name ILIKE :name", nativeQuery = true)
    Optional<Major> findDuplicateMajorWithoutId(@Param(("name")) String name);

    @Query("SELECT m FROM Major m WHERE m.name LIKE %:name%")
    Page<Major> findByName(@Param("name") String name, Pageable pageable);

    @Query(value = "select * from major where name ILIKE :name and id <> :id", nativeQuery = true)
    Optional<Major> findDuplicateMajorWithId(@Param("id") Long id, @Param(("name")) String name);
}
