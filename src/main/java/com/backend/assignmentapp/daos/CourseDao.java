package com.backend.assignmentapp.daos;

import com.backend.assignmentapp.entities.Course;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
public interface CourseDao extends JpaRepository<Course, Long> {
    @Query("SELECT m FROM Course m where m.name LIKE %:name%")
    Page<Course> findByName(String name, Pageable pageable);

    @Query(value = "select * from course where id <> :id and name ILIKE :name", nativeQuery = true)
    Optional<Course> findDuplicateCourseWithId(@Param("id") Long id, @Param("name") String name);

    @Query(value = "select * from course where name ILIKE :name", nativeQuery = true)
    Optional<Course> findDuplicateCourseWithoutId(@Param("name") String name);

}
