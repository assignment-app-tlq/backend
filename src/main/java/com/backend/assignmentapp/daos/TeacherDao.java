package com.backend.assignmentapp.daos;

import com.backend.assignmentapp.entities.Teacher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TeacherDao extends JpaRepository<Teacher, Long> {
    @Query(value = "SELECT * FROM user WHERE name like %:name% and role_id = 2", nativeQuery = true)
    Page<Teacher> findByName(String name, Pageable pageable);
}
