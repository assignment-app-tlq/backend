package com.backend.assignmentapp.daos;

import com.backend.assignmentapp.entities.Student;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentDao extends JpaRepository<Student, Long> {
    @Query(value = "SELECT * FROM student inner join \"user\" on \"user\".id = student.user_id inner join class on class.id = student.class_id where class.name like %:class% and \"user\".role_id = 3", nativeQuery = true)
    Page<Student> findAll(@Param("class") String className, Pageable pageable);

    @Query(value = "SELECT * FROM student inner join \"user\" on \"user\".id = student.user_id inner join class on class.id = student.class_id where class.name like %:class% and user.first_name like %:name% or user.last_name like %:name% and \"user\".role_id = 3", nativeQuery = true)
    Page<Student> findAll(@Param("name") String name,@Param("class") String className, Pageable pageable);

    @Query(value = "SELECT * FROM student inner join \"user\" on \"user\".id = student.user_id WHERE user.first_name like %:name% or user.last_name like %:name% and \"user\".role_id = 3", nativeQuery = true)
    Page<Student> findByStudentName(@Param("name") String name, Pageable pageable);
}