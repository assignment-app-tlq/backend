package com.backend.assignmentapp.daos;

import com.backend.assignmentapp.entities.Division;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DivisionDao extends JpaRepository<Division, Long> {
    @Query(value = "SELECT * FROM division inner join subject on division.subject_id = subject.id inner join class on division.class_id = class.id inner join teacher on division.teacher_id = teacher.id inner join \"user\" on \"user\".id = teacher.user_id WHERE class.name LIKE %:class% AND subject.name LIKE %:subject% AND \"user\".first_name LIKE %:name% AND \"user\".last_name LIKE %:name%", nativeQuery = true)
    Page<Division> findAll(@Param("subject") String subjectName, @Param("class") String className, @Param("name") String teacherName, Pageable pageable);

    @Query(value = "SELECT * FROM division WHERE id <> :id AND subject_id = :subject AND class_id = :class AND teacher_id = :teacher", nativeQuery = true)
    Optional<Object> findDuplicateRoleWithId(@Param("id") Long id, @Param("subject") Long subjectId, @Param("class") Long classId, @Param("teacher") Long teacherId);

    @Query(value = "SELECT * FROM division WHERE subject_id = :subject AND class_id = :class AND teacher_id = :teacher", nativeQuery = true)
    Optional<Object> findDuplicateRoleWithoutId(@Param("subject") Long subjectId, @Param("class") Long classId, @Param("teacher") Long teacherId);
}
