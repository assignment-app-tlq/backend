package com.backend.assignmentapp.daos;

import com.backend.assignmentapp.entities.Assignment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AssignmentDao extends JpaRepository<Assignment, Long> {
    @Query(value = "SELECT * FROM assignment WHERE assignment.name LIKE %:name%", nativeQuery = true)
    Page<Assignment> findAll(@Param("name") String name, Pageable pageable);
}
