package com.backend.assignmentapp.daos;

import com.backend.assignmentapp.entities.Subject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SubjectDao extends JpaRepository<Subject, Long> {
    @Query("SELECT m FROM Subject m WHERE m.name LIKE %:name%")
    Page<Subject> findAll(@Param("name") String name, Pageable pageable);

    @Query(value = "SELECT * FROM Subject WHERE name ILIKE :name", nativeQuery = true)
    Optional<Object> findDuplicateSubjectWithoutId(@Param("name") String name);

    @Query(value = "SELECT * FROM Subject WHERE name ILIKE :name AND id <> :id", nativeQuery = true)
    Optional<Object> findDuplicateSubjectWithId(@Param("id") Long id, @Param("name") String name);
}
