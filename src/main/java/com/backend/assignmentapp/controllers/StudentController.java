package com.backend.assignmentapp.controllers;

import com.backend.assignmentapp.dtos.requests.StudentUserRequest;
import com.backend.assignmentapp.dtos.responses.StudentResponse;
import com.backend.assignmentapp.exceptions.DuplicateException;
import com.backend.assignmentapp.exceptions.InvalidInputException;
import com.backend.assignmentapp.exceptions.NullReferenceException;
import com.backend.assignmentapp.exceptions.ResourceNotFoundException;
import com.backend.assignmentapp.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/student")
public class StudentController {
    private final StudentService studentService;

    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping
    public ResponseEntity<Page<StudentResponse>> getAll(@RequestParam(required = false) String name,
                                                        @RequestParam(value = "class", required = false) String className,
                                                        Pageable pageable) {
        Page<StudentResponse> studentResponsePage = studentService.getAll(name, className, pageable);
        return new ResponseEntity<>(studentResponsePage, HttpStatus.OK);
    }

    @GetMapping("{id}")
    public ResponseEntity<StudentResponse> getById(@PathVariable("id") Long id) throws ResourceNotFoundException {
        StudentResponse studentResponse = studentService.get(id);
        return new ResponseEntity<>(studentResponse, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<StudentResponse> create(@RequestBody StudentUserRequest studentUserRequest) throws InvalidInputException, DuplicateException, ResourceNotFoundException, NullReferenceException {
        StudentResponse studentResponse = studentService.save(studentUserRequest);
        return new ResponseEntity<>(studentResponse, HttpStatus.CREATED);
    }

    @PutMapping("{id}")
    public ResponseEntity<StudentResponse> updateById(@PathVariable("id") Long id, @RequestBody StudentUserRequest studentUserRequest) throws InvalidInputException, DuplicateException, ResourceNotFoundException, NullReferenceException {
        StudentResponse studentResponse = studentService.update(id, studentUserRequest);
        return new ResponseEntity<>(studentResponse, HttpStatus.OK);
    }

    @DeleteMapping
    public ResponseEntity<String> deleteMultiByIds(@RequestParam("id") List<Long> ids) throws ResourceNotFoundException {
        studentService.deleteMulti(ids);
        return new ResponseEntity<>("Deleted", HttpStatus.OK);
    }
}
