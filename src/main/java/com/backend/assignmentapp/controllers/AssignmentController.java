package com.backend.assignmentapp.controllers;

import com.backend.assignmentapp.dtos.requests.AssignmentRequest;
import com.backend.assignmentapp.dtos.responses.AssignmentResponse;
import com.backend.assignmentapp.exceptions.ResourceNotFoundException;
import com.backend.assignmentapp.services.AssignmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/assignment")
public class AssignmentController {
    private final AssignmentService assignmentService;

    @Autowired
    public AssignmentController(AssignmentService assignmentService) {
        this.assignmentService = assignmentService;
    }

    @GetMapping
    public ResponseEntity<Page<AssignmentResponse>> getAll(@RequestParam(required = false) String name, Pageable pageable) {
        Page<AssignmentResponse> assignmentResponse = assignmentService.getAll(name, pageable);
        return new ResponseEntity<>(assignmentResponse, HttpStatus.OK);
    }

    @GetMapping("{id}")
    public ResponseEntity<AssignmentResponse> getById(@PathVariable("id") Long id) {
        AssignmentResponse assignmentResponse = assignmentService.get(id);
        return new ResponseEntity<>(assignmentResponse, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<AssignmentResponse> create(@ModelAttribute AssignmentRequest assignmentRequest) throws IOException, ResourceNotFoundException {
        AssignmentResponse assignmentResponse = assignmentService.create(assignmentRequest);
        return new ResponseEntity<>(assignmentResponse, HttpStatus.CREATED);
    }

    @PutMapping("{id}")
    public ResponseEntity<AssignmentResponse> updateById(@PathVariable("id") Long id, @ModelAttribute AssignmentRequest assignmentRequest) {
        AssignmentResponse assignmentResponse = assignmentService.update(id, assignmentRequest);
        return new ResponseEntity<>(assignmentResponse, HttpStatus.OK);
    }

    @DeleteMapping
    public ResponseEntity<String> deleteMultiByIds(@RequestParam("id") List<Long> ids) {
        assignmentService.deleteMulti(ids);
        return new ResponseEntity<>("Deleted", HttpStatus.OK);
    }
}
