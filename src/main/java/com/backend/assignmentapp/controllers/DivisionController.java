package com.backend.assignmentapp.controllers;

import com.backend.assignmentapp.dtos.requests.DivisionRequest;
import com.backend.assignmentapp.dtos.responses.DivisionResponse;
import com.backend.assignmentapp.exceptions.DuplicateException;
import com.backend.assignmentapp.exceptions.ResourceNotFoundException;
import com.backend.assignmentapp.services.DivisionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/division")
public class DivisionController {
    private final DivisionService divisionService;

    @Autowired
    public DivisionController(DivisionService divisionService) {
        this.divisionService = divisionService;
    }

    @GetMapping
    public ResponseEntity<Page<DivisionResponse>> getAll(@RequestParam(value = "subject", required = false) String subjectName,
                                                         @RequestParam(value = "class", required = false) String className,
                                                         @RequestParam(value = "teacher", required = false) String teacherName,
                                                         Pageable pageable
                                                         ) {
        Page<DivisionResponse> divisionResponse = divisionService.getAll(subjectName, className, teacherName, pageable);
        return new ResponseEntity<>(divisionResponse, HttpStatus.OK);
    }

    @GetMapping("{id}")
    public ResponseEntity<DivisionResponse> getById(@PathVariable("id") Long id) throws ResourceNotFoundException {
        DivisionResponse divisionResponse = divisionService.get(id);
        return new ResponseEntity<>(divisionResponse, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<DivisionResponse> create(@RequestBody DivisionRequest divisionRequest) throws DuplicateException, ResourceNotFoundException {
        DivisionResponse divisionResponse = divisionService.create(divisionRequest);
        return new ResponseEntity<>(divisionResponse, HttpStatus.CREATED);
    }

    @PutMapping("{id}")
    public ResponseEntity<DivisionResponse> updateById(@PathVariable("id") Long id, @RequestBody DivisionRequest divisionRequest) throws DuplicateException, ResourceNotFoundException {
        DivisionResponse divisionResponse = divisionService.update(id, divisionRequest);
        return new ResponseEntity<>(divisionResponse, HttpStatus.OK);
    }

    @DeleteMapping
    public ResponseEntity<String> deleteMultiByIds(@RequestParam("id") List<Long> ids) throws ResourceNotFoundException {
        divisionService.deleteMulti(ids);
        return new ResponseEntity<>("Deleted", HttpStatus.OK);
    }
}
