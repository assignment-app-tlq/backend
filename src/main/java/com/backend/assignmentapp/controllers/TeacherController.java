package com.backend.assignmentapp.controllers;

import com.backend.assignmentapp.dtos.requests.TeacherRequest;
import com.backend.assignmentapp.dtos.requests.TeacherUserRequest;
import com.backend.assignmentapp.dtos.responses.TeacherResponse;
import com.backend.assignmentapp.exceptions.DuplicateException;
import com.backend.assignmentapp.exceptions.InvalidInputException;
import com.backend.assignmentapp.exceptions.NullReferenceException;
import com.backend.assignmentapp.exceptions.ResourceNotFoundException;
import com.backend.assignmentapp.services.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("api/teacher")
public class TeacherController {
    private final TeacherService teacherService;

    @Autowired
    public TeacherController(TeacherService teacherService) {
        this.teacherService = teacherService;
    }

    @GetMapping
    public ResponseEntity<Page<TeacherResponse>> getAll(@RequestParam(required = false) String name, Pageable pageable) {
        Page<TeacherResponse> teacherResponsePage = teacherService.getAll(name, pageable);
        return new ResponseEntity<>(teacherResponsePage, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<TeacherResponse> create(@RequestBody TeacherUserRequest teacherUserRequest) throws InvalidInputException, DuplicateException, ResourceNotFoundException, NullReferenceException {
        TeacherResponse teacherResponse = teacherService.create(teacherUserRequest);
        return new ResponseEntity<>(teacherResponse, HttpStatus.CREATED);
    }

    @GetMapping("{id}")
    public ResponseEntity<TeacherResponse> getById(@PathVariable("id") Long id) throws ResourceNotFoundException {
        TeacherResponse teacherResponse = teacherService.get(id);
        return new ResponseEntity<>(teacherResponse, HttpStatus.OK);
    }

    @PutMapping("{id}")
    public ResponseEntity<TeacherResponse> updateById(@PathVariable("id") Long id, @RequestBody TeacherUserRequest teacherUserRequest) throws InvalidInputException, DuplicateException, ResourceNotFoundException, NullReferenceException {
        TeacherResponse teacherResponse = teacherService.update(id, teacherUserRequest);
        return new ResponseEntity<>(teacherResponse, HttpStatus.OK);
    }

    @DeleteMapping
    public ResponseEntity<String> deleteMultiByIds(@RequestParam("id") List<Long> ids) throws ResourceNotFoundException {
        teacherService.deleteMulti(ids);
        return new ResponseEntity<>("Deleted", HttpStatus.OK);
    }
}
