package com.backend.assignmentapp.controllers;

import com.backend.assignmentapp.dtos.requests.CourseRequest;
import com.backend.assignmentapp.dtos.responses.CourseResponse;
import com.backend.assignmentapp.exceptions.DuplicateException;
import com.backend.assignmentapp.exceptions.InvalidInputException;
import com.backend.assignmentapp.exceptions.ResourceNotFoundException;
import com.backend.assignmentapp.services.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/course")
public class CourseController {
    private final CourseService courseService;

    @Autowired
    public CourseController(CourseService courseService) {
        this.courseService = courseService;
    }

    @GetMapping
    public ResponseEntity<Page<CourseResponse>> getAll(@RequestParam(required = false) String name,
                                                       Pageable pageable) {
        Page<CourseResponse> courseResponses = courseService.getAll(name, pageable);
        return new ResponseEntity<>(courseResponses, HttpStatus.OK);
    }

    @GetMapping("{id}")
    public ResponseEntity<CourseResponse> getById(@PathVariable(value = "id") Long id) throws ResourceNotFoundException {
        CourseResponse courseResponse = courseService.get(id);
        return new ResponseEntity<>(courseResponse, HttpStatus.OK);
    }


    @PostMapping
    public ResponseEntity<CourseResponse> create(@RequestBody CourseRequest courseRequest) throws DuplicateException, InvalidInputException {
        CourseResponse courseResponse = courseService.create(courseRequest);
        return new ResponseEntity<>(courseResponse, HttpStatus.CREATED);
    }

    @DeleteMapping
    public ResponseEntity<String> deleteMultiByIds(@RequestParam("id") List<Long> ids) throws ResourceNotFoundException {
        courseService.deleteMulti(ids);
        return new ResponseEntity<>("Deleted", HttpStatus.OK);
    }

    @PutMapping("{id}")
    public ResponseEntity<CourseResponse> updateById(@PathVariable("id") Long id, @RequestBody CourseRequest courseRequest) throws InvalidInputException, DuplicateException, ResourceNotFoundException {
        CourseResponse courseResponse = courseService.update(id, courseRequest);
        return new ResponseEntity<>(courseResponse, HttpStatus.OK);
    }
}
