package com.backend.assignmentapp.controllers;

import com.backend.assignmentapp.dtos.requests.ClassRequest;
import com.backend.assignmentapp.dtos.responses.ClassResponse;
import com.backend.assignmentapp.exceptions.DuplicateException;
import com.backend.assignmentapp.exceptions.InvalidInputException;
import com.backend.assignmentapp.exceptions.NullReferenceException;
import com.backend.assignmentapp.exceptions.ResourceNotFoundException;
import com.backend.assignmentapp.services.ClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/class")
public class ClassController {
    private final ClassService classService;

    @Autowired
    public ClassController(ClassService classService) {
        this.classService = classService;
    }

    @GetMapping
    public ResponseEntity<Page<ClassResponse>> getAll(@RequestParam(required = false) String name,
                                                      Pageable pageable) {
        Page<ClassResponse> classResponse = classService.getAll(name, pageable);
        return new ResponseEntity<>(classResponse, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<ClassResponse> create(@RequestBody ClassRequest classRequest) throws InvalidInputException, ResourceNotFoundException, DuplicateException, NullReferenceException {
        ClassResponse classResponse = classService.create(classRequest);
        return new ResponseEntity<>(classResponse, HttpStatus.CREATED);
    }

    @GetMapping("{id}")
    public ResponseEntity<ClassResponse> getById(@PathVariable("id") Long id) throws ResourceNotFoundException {
        ClassResponse classResponse = classService.get(id);
        return new ResponseEntity<>(classResponse, HttpStatus.OK);
    }

    @PutMapping("{id}")
    public ResponseEntity<ClassResponse> updateById(@PathVariable("id") Long id, @RequestBody ClassRequest classRequest) throws InvalidInputException, DuplicateException, ResourceNotFoundException, NullReferenceException {
        ClassResponse classResponse = classService.update(id, classRequest);
        return new ResponseEntity<>(classResponse, HttpStatus.OK);
    }

    @DeleteMapping
    public ResponseEntity<String> deleteMultiByIds(@RequestParam("id") List<Long> ids) throws ResourceNotFoundException {
        classService.deleteMulti(ids);
        return new ResponseEntity<>("Deleted", HttpStatus.OK);
    }
}
