package com.backend.assignmentapp.controllers;

import com.backend.assignmentapp.dtos.requests.UserRequest;
import com.backend.assignmentapp.dtos.responses.UserResponse;
import com.backend.assignmentapp.exceptions.DuplicateException;
import com.backend.assignmentapp.exceptions.InvalidInputException;
import com.backend.assignmentapp.exceptions.NullReferenceException;
import com.backend.assignmentapp.exceptions.ResourceNotFoundException;
import com.backend.assignmentapp.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("api/user")
public class UserController {
    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public ResponseEntity<Page<UserResponse>> getAll(@RequestParam(required = false) String name, Pageable pageable) {
        Page<UserResponse> userResponsePage = userService.getAll(name, pageable);
        return new ResponseEntity<>(userResponsePage, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<UserResponse> create(@RequestBody UserRequest userRequest) throws InvalidInputException, DuplicateException, ResourceNotFoundException, NullReferenceException {
        UserResponse userResponse = userService.create(userRequest);
        return new ResponseEntity<>(userResponse, HttpStatus.CREATED);
    }

    @GetMapping("{id}")
    public ResponseEntity<UserResponse> getById(@PathVariable("id") Long id) throws ResourceNotFoundException {
        UserResponse userResponse = userService.get(id);
        return new ResponseEntity<>(userResponse, HttpStatus.OK);
    }

    @PutMapping("{id}")
    public ResponseEntity<UserResponse> updateById(@PathVariable("id") Long id, @RequestBody UserRequest userRequest) throws ResourceNotFoundException, InvalidInputException, DuplicateException, NullReferenceException {
        UserResponse userResponse = userService.update(id, userRequest);
        return new ResponseEntity<>(userResponse, HttpStatus.OK);
    }

    @DeleteMapping
    public ResponseEntity<String> deleteMultiByIds(@RequestParam("id") List<Long> ids) throws ResourceNotFoundException {
        userService.deleteMulti(ids);
        return new ResponseEntity<>("Deleted", HttpStatus.OK);
    }
}
