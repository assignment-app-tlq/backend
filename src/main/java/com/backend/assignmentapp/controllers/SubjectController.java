package com.backend.assignmentapp.controllers;

import com.backend.assignmentapp.dtos.requests.SubjectRequest;
import com.backend.assignmentapp.dtos.responses.SubjectResponse;
import com.backend.assignmentapp.exceptions.DuplicateException;
import com.backend.assignmentapp.exceptions.InvalidInputException;
import com.backend.assignmentapp.exceptions.ResourceNotFoundException;
import com.backend.assignmentapp.services.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("api/subject")
public class SubjectController {
    private final SubjectService subjectService;

    @Autowired
    public SubjectController(SubjectService subjectService) {
        this.subjectService = subjectService;
    }

    @GetMapping
    public ResponseEntity<Page<SubjectResponse>> getAll(@RequestParam(required = false) String name, Pageable pageable) {
        Page<SubjectResponse> subjectResponsePage = subjectService.getAll(name, pageable);
        return new ResponseEntity<>(subjectResponsePage, HttpStatus.OK);
    }

    @GetMapping("{id}")
    public ResponseEntity<SubjectResponse> getById(@PathVariable("id") Long id) throws ResourceNotFoundException {
        SubjectResponse subjectResponse = subjectService.get(id);
        return new ResponseEntity<>(subjectResponse, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<SubjectResponse> create(@RequestBody SubjectRequest subjectRequest) throws InvalidInputException, DuplicateException {
        SubjectResponse subjectResponse = subjectService.create(subjectRequest);
        return new ResponseEntity<>(subjectResponse, HttpStatus.CREATED);
    }

    @PutMapping("{id}")
    public ResponseEntity<SubjectResponse> updateById(@PathVariable("id") Long id, @RequestBody SubjectRequest subjectRequest) throws InvalidInputException, DuplicateException, ResourceNotFoundException {
        SubjectResponse subjectResponse = subjectService.update(id, subjectRequest);
        return new ResponseEntity<>(subjectResponse, HttpStatus.OK);
    }

    @DeleteMapping
    public ResponseEntity<String> deleteMultiByIds(@RequestParam("id") List<Long> ids) throws ResourceNotFoundException {
        subjectService.deleteMulti(ids);
        return new ResponseEntity<>("Deleted", HttpStatus.OK);
    }
}
