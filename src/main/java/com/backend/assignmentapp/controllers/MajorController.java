package com.backend.assignmentapp.controllers;

import com.backend.assignmentapp.dtos.responses.MajorResponse;
import com.backend.assignmentapp.exceptions.DuplicateException;
import com.backend.assignmentapp.exceptions.InvalidInputException;
import com.backend.assignmentapp.exceptions.ResourceNotFoundException;
import com.backend.assignmentapp.services.MajorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.backend.assignmentapp.dtos.requests.MajorRequest;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/major")
public class MajorController {
    private final MajorService majorService;

    @Autowired
    public MajorController(MajorService majorService) {
        this.majorService = majorService;
    }

    @GetMapping
    public ResponseEntity<Page<MajorResponse>> getAll(@RequestParam(required = false) String name, Pageable pageable) {
        Page<MajorResponse> majorResponse = majorService.getAll(name, pageable);
        return new ResponseEntity<>(majorResponse, HttpStatus.OK);
    }

    @GetMapping("{id}")
    public ResponseEntity<MajorResponse> getById(@PathVariable("id") Long id) throws ResourceNotFoundException {
        MajorResponse majorResponse = majorService.get(id);
        return new ResponseEntity<>(majorResponse, HttpStatus.OK);
    }
    @PostMapping
    public ResponseEntity<MajorResponse> create(@RequestBody MajorRequest majorRequest) throws InvalidInputException, DuplicateException {
        MajorResponse majorResponse = majorService.create(majorRequest);
        return new ResponseEntity<>(majorResponse, HttpStatus.CREATED);
    }

    @PutMapping("{id}")
    public ResponseEntity<MajorResponse> updateById(@PathVariable("id") Long id, @RequestBody MajorRequest majorRequest) throws InvalidInputException, DuplicateException, ResourceNotFoundException {
        MajorResponse majorResponse = majorService.update(id, majorRequest);
        return new ResponseEntity<>(majorResponse, HttpStatus.OK);
    }

    @DeleteMapping
    public ResponseEntity<String> deleteMultiByIds(@RequestParam("id") List<Long> ids) throws ResourceNotFoundException {
        majorService.deleteMulti(ids);
        return new ResponseEntity<>("Deleted", HttpStatus.OK);
    }
}
