package com.backend.assignmentapp.services;

import com.backend.assignmentapp.dtos.requests.TeacherRequest;
import com.backend.assignmentapp.dtos.requests.TeacherUserRequest;
import com.backend.assignmentapp.dtos.responses.TeacherResponse;
import com.backend.assignmentapp.entities.User;
import com.backend.assignmentapp.exceptions.DuplicateException;
import com.backend.assignmentapp.exceptions.InvalidInputException;
import com.backend.assignmentapp.exceptions.NullReferenceException;
import com.backend.assignmentapp.exceptions.ResourceNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface TeacherService {
    Page<TeacherResponse> getAll(String name, Pageable pageable);

    TeacherResponse create(TeacherUserRequest teacherUserRequest) throws InvalidInputException, DuplicateException, ResourceNotFoundException, NullReferenceException;

    TeacherResponse get(Long id) throws ResourceNotFoundException;

    TeacherResponse update(Long id, TeacherUserRequest teacherUserRequest) throws InvalidInputException, DuplicateException, ResourceNotFoundException, NullReferenceException;

    void deleteMulti(List<Long> ids) throws ResourceNotFoundException;
}
