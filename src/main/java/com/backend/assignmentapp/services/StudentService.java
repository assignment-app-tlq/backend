package com.backend.assignmentapp.services;

import com.backend.assignmentapp.dtos.requests.StudentRequest;
import com.backend.assignmentapp.dtos.requests.StudentUserRequest;
import com.backend.assignmentapp.dtos.responses.StudentResponse;
import com.backend.assignmentapp.exceptions.DuplicateException;
import com.backend.assignmentapp.exceptions.InvalidInputException;
import com.backend.assignmentapp.exceptions.NullReferenceException;
import com.backend.assignmentapp.exceptions.ResourceNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface StudentService {
    Page<StudentResponse> getAll(String name, String className, Pageable pageable);

    StudentResponse get(Long id) throws ResourceNotFoundException;

    StudentResponse save(StudentUserRequest studentUserRequest) throws InvalidInputException, DuplicateException, ResourceNotFoundException, NullReferenceException;

    StudentResponse update(Long id, StudentUserRequest studentUserRequest) throws InvalidInputException, DuplicateException, ResourceNotFoundException, NullReferenceException;

    void deleteMulti(List<Long> ids) throws ResourceNotFoundException;
}
