package com.backend.assignmentapp.services;

import com.backend.assignmentapp.dtos.requests.SubjectRequest;
import com.backend.assignmentapp.dtos.responses.SubjectResponse;
import com.backend.assignmentapp.exceptions.DuplicateException;
import com.backend.assignmentapp.exceptions.InvalidInputException;
import com.backend.assignmentapp.exceptions.ResourceNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface SubjectService {

    Page<SubjectResponse> getAll(String name, Pageable pageable);

    SubjectResponse get(Long id) throws ResourceNotFoundException;

    SubjectResponse create(SubjectRequest subjectRequest) throws DuplicateException, InvalidInputException;

    SubjectResponse update(Long id, SubjectRequest subjectRequest) throws DuplicateException, InvalidInputException, ResourceNotFoundException;

    void deleteMulti(List<Long> ids) throws ResourceNotFoundException;
}
