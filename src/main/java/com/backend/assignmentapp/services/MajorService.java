package com.backend.assignmentapp.services;

import com.backend.assignmentapp.dtos.responses.MajorResponse;
import com.backend.assignmentapp.exceptions.DuplicateException;
import com.backend.assignmentapp.exceptions.InvalidInputException;
import com.backend.assignmentapp.exceptions.ResourceNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.backend.assignmentapp.dtos.requests.MajorRequest;

import java.util.List;

public interface MajorService {
    Page<MajorResponse> getAll(String name, Pageable pageable);

    MajorResponse get(Long id) throws ResourceNotFoundException;

    MajorResponse create(MajorRequest majorRequest) throws DuplicateException, InvalidInputException;

    MajorResponse update(Long id, MajorRequest majorRequest) throws ResourceNotFoundException, DuplicateException, InvalidInputException;

    void deleteMulti(List<Long> ids) throws ResourceNotFoundException;
}
