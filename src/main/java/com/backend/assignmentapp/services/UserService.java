package com.backend.assignmentapp.services;

import com.backend.assignmentapp.dtos.requests.UserRequest;
import com.backend.assignmentapp.dtos.responses.UserResponse;
import com.backend.assignmentapp.entities.User;
import com.backend.assignmentapp.exceptions.DuplicateException;
import com.backend.assignmentapp.exceptions.InvalidInputException;
import com.backend.assignmentapp.exceptions.NullReferenceException;
import com.backend.assignmentapp.exceptions.ResourceNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface UserService {
    Page<UserResponse> getAll(String name, Pageable pageable);

    UserResponse create(UserRequest userRequest) throws ResourceNotFoundException, NullReferenceException, DuplicateException, InvalidInputException;

    UserResponse get(Long id) throws ResourceNotFoundException;

    UserResponse update(Long id, UserRequest userRequest) throws ResourceNotFoundException, DuplicateException, InvalidInputException, NullReferenceException;

    void deleteMulti(List<Long> ids) throws ResourceNotFoundException;

    User createUser(UserRequest userRequest) throws NullReferenceException, ResourceNotFoundException, DuplicateException, InvalidInputException;

    User updateUser(Long id, UserRequest userRequest) throws ResourceNotFoundException, NullReferenceException, DuplicateException, InvalidInputException;
}
