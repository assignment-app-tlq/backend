package com.backend.assignmentapp.services;


import com.backend.assignmentapp.dtos.requests.CourseRequest;
import com.backend.assignmentapp.dtos.responses.CourseResponse;
import com.backend.assignmentapp.exceptions.DuplicateException;
import com.backend.assignmentapp.exceptions.InvalidInputException;
import com.backend.assignmentapp.exceptions.NullReferenceException;
import com.backend.assignmentapp.exceptions.ResourceNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface CourseService {
    Page<CourseResponse> getAll(String name, Pageable pageable);

    CourseResponse get(Long id) throws ResourceNotFoundException;

    CourseResponse update(Long id, CourseRequest courseRequest) throws DuplicateException, InvalidInputException, ResourceNotFoundException;

    CourseResponse create(CourseRequest courseRequest) throws DuplicateException, InvalidInputException;

    void deleteMulti(List<Long> ids) throws ResourceNotFoundException;
}
