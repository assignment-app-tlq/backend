package com.backend.assignmentapp.services;

import com.backend.assignmentapp.dtos.requests.RoleRequest;
import com.backend.assignmentapp.dtos.responses.RoleResponse;
import com.backend.assignmentapp.exceptions.DuplicateException;
import com.backend.assignmentapp.exceptions.InvalidInputException;
import com.backend.assignmentapp.exceptions.NullReferenceException;
import com.backend.assignmentapp.exceptions.ResourceNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface RoleService {
    Page<RoleResponse> getAll(String name, Pageable paging);

    RoleResponse create(RoleRequest roleRequest) throws DuplicateException, InvalidInputException;

    RoleResponse get(Long id) throws ResourceNotFoundException;

    RoleResponse update(Long id, RoleRequest request) throws ResourceNotFoundException, DuplicateException, NullReferenceException, InvalidInputException;

    void deleteMulti(List<Long> ids) throws ResourceNotFoundException;
}

