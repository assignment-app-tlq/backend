package com.backend.assignmentapp.services;

import com.backend.assignmentapp.dtos.requests.DivisionRequest;
import com.backend.assignmentapp.dtos.responses.DivisionResponse;
import com.backend.assignmentapp.exceptions.DuplicateException;
import com.backend.assignmentapp.exceptions.ResourceNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface DivisionService {
    Page<DivisionResponse> getAll(String subjectName, String className, String teacherName, Pageable pageable);

    DivisionResponse get(Long id) throws ResourceNotFoundException;

    DivisionResponse create(DivisionRequest divisionRequest) throws ResourceNotFoundException, DuplicateException;

    void deleteMulti(List<Long> ids) throws ResourceNotFoundException;

    DivisionResponse update(Long id, DivisionRequest divisionRequest) throws ResourceNotFoundException, DuplicateException;
}
