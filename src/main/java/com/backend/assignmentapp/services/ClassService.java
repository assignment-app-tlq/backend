package com.backend.assignmentapp.services;

import com.backend.assignmentapp.dtos.requests.ClassRequest;
import com.backend.assignmentapp.dtos.responses.ClassResponse;
import com.backend.assignmentapp.exceptions.DuplicateException;
import com.backend.assignmentapp.exceptions.InvalidInputException;
import com.backend.assignmentapp.exceptions.NullReferenceException;
import com.backend.assignmentapp.exceptions.ResourceNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ClassService {
    Page<ClassResponse> getAll(String name, Pageable pageable);

    ClassResponse create(ClassRequest classRequest) throws ResourceNotFoundException, InvalidInputException, DuplicateException, NullReferenceException;

    ClassResponse get(Long id) throws ResourceNotFoundException;

    ClassResponse update(Long id, ClassRequest classRequest) throws ResourceNotFoundException, DuplicateException, InvalidInputException, NullReferenceException;

    void deleteMulti(List<Long> ids) throws ResourceNotFoundException;
}
