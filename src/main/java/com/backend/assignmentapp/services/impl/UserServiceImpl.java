package com.backend.assignmentapp.services.impl;

import com.backend.assignmentapp.daos.RoleDao;
import com.backend.assignmentapp.daos.UserDao;
import com.backend.assignmentapp.dtos.requests.StudentRequest;
import com.backend.assignmentapp.dtos.requests.StudentUserRequest;
import com.backend.assignmentapp.dtos.requests.UserRequest;
import com.backend.assignmentapp.dtos.responses.UserResponse;
import com.backend.assignmentapp.entities.User;
import com.backend.assignmentapp.exceptions.DuplicateException;
import com.backend.assignmentapp.exceptions.InvalidInputException;
import com.backend.assignmentapp.exceptions.NullReferenceException;
import com.backend.assignmentapp.exceptions.ResourceNotFoundException;
import com.backend.assignmentapp.modelmappers.UserMapper;
import com.backend.assignmentapp.models.CustomError;
import com.backend.assignmentapp.services.TeacherService;
import com.backend.assignmentapp.services.UserService;
import com.backend.assignmentapp.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    private final UserDao userDao;

    private final UserMapper userMapper;

    private final RoleDao roleDao;

    private final UserValidator userValidator;

    @Autowired
    public UserServiceImpl(UserDao userDao, UserMapper userMapper, RoleDao roleDao, UserValidator userValidator) {
        this.userDao = userDao;
        this.userMapper = userMapper;
        this.roleDao = roleDao;
        this.userValidator = userValidator;
    }

    @Override
    public Page<UserResponse> getAll(String name, Pageable pageable) {
        Page<User> pageUsers;
        if(name == null) {
            pageUsers = userDao.findAll(pageable);
        } else {
            pageUsers = userDao.findByName(name, pageable);
        }
        Page<UserResponse> userResponses = pageUsers.map(userMapper::convertToResponse);
        return userResponses;
    }

    @Override
    public UserResponse create(UserRequest userRequest) throws ResourceNotFoundException, NullReferenceException, DuplicateException, InvalidInputException {
        User userSave = createUser(userRequest);
        return userMapper.convertToResponse(userSave);
    }

    @Override
    public UserResponse get(Long id) throws ResourceNotFoundException {
        Optional<User> userOptional = userDao.findById(id);
        if(userOptional.isEmpty()) {
            throw new ResourceNotFoundException("User not found with id = " + id);
        }
        User user = userOptional.get();
        UserResponse userResponse = userMapper.convertToResponse(user);
        return userResponse;
    }

    @Override
    public UserResponse update(Long id, UserRequest userRequest) throws ResourceNotFoundException, DuplicateException, InvalidInputException, NullReferenceException {
        User userSave = this.updateUser(id, userRequest);
        UserResponse userResponse = userMapper.convertToResponse(userSave);
        return userResponse;
    }

    @Override
    public void deleteMulti(List<Long> ids) throws ResourceNotFoundException {
        List<String> idList = new ArrayList<>();
        for (Long id : ids) {
            if(userDao.findById(id).isEmpty()) {
                idList.add(String.valueOf(id));
            }
        }
        if(!idList.isEmpty()) {
            throw new ResourceNotFoundException("User not found with id = " + String.join(",", idList));
        }
        userDao.deleteAllByIdInBatch(ids);
    }

    @Override
    public User createUser(UserRequest userRequest) throws NullReferenceException, ResourceNotFoundException, DuplicateException, InvalidInputException {
        userRequest.setFirstName(userRequest.getFirstName().replaceAll("\\s\\s+" , " ").trim());
        userRequest.setLastName(userRequest.getLastName().replaceAll("\\s\\s+" , " ").trim());
        userRequest.setEmail(userRequest.getEmail().replaceAll("\\s\\s+" , " ").trim());
        if(userRequest.getRoleId() == null) {
            throw new NullReferenceException("Role ID cannot be null");
        }
        if(!roleDao.existsById(userRequest.getRoleId())) {
            throw new ResourceNotFoundException("Role not found with id = " + userRequest.getRoleId());
        }
        if(userDao.findDuplicateEmailWithoutId(userRequest.getEmail()).isPresent()) {
            throw new DuplicateException(userRequest.getEmail() + " is used");
        }
        List<CustomError> listErrors = new ArrayList<>();
        if(!userValidator.checkFormatName(userRequest.getFirstName()).isEmpty() || !userValidator.checkFormatName(userRequest.getLastName()).isEmpty()) {
            listErrors.addAll(userValidator.checkFormatName(userRequest.getFirstName()));
        }
        if(userValidator.checkFormatEmailAddress(userRequest.getEmail()) != null) {
            listErrors.add(userValidator.checkFormatEmailAddress(userRequest.getEmail()));
        }
        if(!userValidator.checkFormatPassword(userRequest.getPassword()).isEmpty()) {
            listErrors.addAll(userValidator.checkFormatPassword(userRequest.getPassword()));
        }
        if(!listErrors.isEmpty()) {
            throw new InvalidInputException(listErrors);
        }
        User user = userMapper.convertToEntity(userRequest);
        User userSave = userDao.save(user);
        return userSave;
    }

    @Override
    public User updateUser(Long id, UserRequest userRequest) throws ResourceNotFoundException, NullReferenceException, DuplicateException, InvalidInputException {
        userRequest.setFirstName(userRequest.getFirstName().replaceAll("\\s\\s+" , " ").trim());
        userRequest.setLastName(userRequest.getLastName().replaceAll("\\s\\s+" , " ").trim());
        userRequest.setEmail(userRequest.getEmail().replaceAll("\\s\\s+" , " ").trim());
        Optional<User> userOptional = userDao.findById(id);
        if(userOptional.isEmpty()) {
            throw new ResourceNotFoundException("User not found with id = " + id);
        }
        userRequest.setFirstName(userRequest.getFirstName().trim());
        userRequest.setLastName(userRequest.getLastName().trim());
        userRequest.setRoleId(userDao.findById(id).get().getRole().getId());
        if(userRequest.getRoleId() == null) {
            throw new NullReferenceException("Role ID cannot be null");
        }
        if(!roleDao.existsById(userRequest.getRoleId())) {
            throw new ResourceNotFoundException("Role not found with id = " + userRequest.getRoleId());
        }
        if(userDao.findDuplicateEmailWithId(id, userRequest.getEmail()).isPresent()) {
            throw new DuplicateException(userRequest.getEmail() + " is used");
        }
        List<CustomError> listErrors = new ArrayList<>();
        if(!userValidator.checkFormatName(userRequest.getFirstName()).isEmpty() || !userValidator.checkFormatName(userRequest.getLastName()).isEmpty()) {
            listErrors.addAll(userValidator.checkFormatName(userRequest.getFirstName()));
        }
        if(userValidator.checkFormatEmailAddress(userRequest.getEmail()) != null) {
            listErrors.add(userValidator.checkFormatEmailAddress(userRequest.getEmail()));
        }
        if(!userValidator.checkFormatPassword(userRequest.getPassword()).isEmpty()) {
            listErrors.addAll(userValidator.checkFormatPassword(userRequest.getPassword()));
        }
        if(!listErrors.isEmpty()) {
            throw new InvalidInputException(listErrors);
        }
        User user = userMapper.convertToEntity(userRequest);
        user.setId(id);
        User userSave = userDao.save(user);
        return userSave;
    }
}
