package com.backend.assignmentapp.services.impl;

import com.backend.assignmentapp.daos.SubjectDao;
import com.backend.assignmentapp.dtos.requests.SubjectRequest;
import com.backend.assignmentapp.dtos.responses.SubjectResponse;
import com.backend.assignmentapp.entities.Subject;
import com.backend.assignmentapp.exceptions.DuplicateException;
import com.backend.assignmentapp.exceptions.InvalidInputException;
import com.backend.assignmentapp.exceptions.ResourceNotFoundException;
import com.backend.assignmentapp.modelmappers.SubjectMapper;
import com.backend.assignmentapp.services.SubjectService;
import com.backend.assignmentapp.validator.SubjectValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class SubjectServiceImpl implements SubjectService {
    private final SubjectDao subjectDao;
    private final SubjectMapper subjectMapper;
    private final SubjectValidator subjectValidator;

    @Autowired
    public SubjectServiceImpl(SubjectDao subjectDao, SubjectMapper subjectMapper, SubjectValidator subjectValidator) {
        this.subjectDao = subjectDao;
        this.subjectMapper = subjectMapper;
        this.subjectValidator = subjectValidator;
    }

    @Override
    public Page<SubjectResponse> getAll(String name, Pageable pageable) {
        Page<Subject> pageSubjects;
        if(name == null) {
            pageSubjects = subjectDao.findAll(pageable);
        } else {
            pageSubjects = subjectDao.findAll(name, pageable);
        }
        Page<SubjectResponse> subjectResponsePage = pageSubjects.map(subjectMapper::convertToResponse);
        return subjectResponsePage;
    }

    @Override
    public SubjectResponse get(Long id) throws ResourceNotFoundException {
        Optional<Subject> subjectOptional = subjectDao.findById(id);
        if(subjectOptional.isEmpty()) {
            throw new ResourceNotFoundException("Subject not found with id = " + id);
        }
        SubjectResponse subjectResponse = subjectMapper.convertToResponse(subjectOptional.get());
        return subjectResponse;
    }

    @Override
    public SubjectResponse create(SubjectRequest subjectRequest) throws DuplicateException, InvalidInputException {
        subjectRequest.setName(subjectRequest.getName().replaceAll("\\s\\s+", " ").trim());
        if(subjectDao.findDuplicateSubjectWithoutId(subjectRequest.getName()).isPresent()) {
            throw new DuplicateException("Subject name already exists");
        }
        if(!subjectValidator.checkFormatName(subjectRequest.getName()).isEmpty()) {
            throw new InvalidInputException(subjectValidator.checkFormatName(subjectRequest.getName()));
        }
        Subject subject = subjectMapper.convertToEntity(subjectRequest);
        Subject saveSubject = subjectDao.save(subject);
        SubjectResponse subjectResponse = subjectMapper.convertToResponse(saveSubject);
        return subjectResponse;
    }

    @Override
    public SubjectResponse update(Long id, SubjectRequest subjectRequest) throws DuplicateException, InvalidInputException, ResourceNotFoundException {
        subjectRequest.setName(subjectRequest.getName().replaceAll("\\s\\s+", " ").trim());
        Optional<Subject> subjectOptional = subjectDao.findById(id);
        if(subjectDao.findDuplicateSubjectWithId(id, subjectRequest.getName()).isPresent()) {
            throw new DuplicateException("Subject name already exists");
        }
        if(!subjectValidator.checkFormatName(subjectRequest.getName()).isEmpty()) {
            throw new InvalidInputException(subjectValidator.checkFormatName(subjectRequest.getName()));
        }
        if(subjectOptional.isEmpty()) {
            throw new ResourceNotFoundException("Subject not found with id = " + id);
        }
        if(!subjectValidator.checkFormatName(subjectRequest.getName()).isEmpty()) {
            throw new InvalidInputException(subjectValidator.checkFormatName(subjectRequest.getName()));
        }
        Subject subject = subjectMapper.convertToEntity(subjectRequest);
        subject.setId(id);
        Subject saveSubject = subjectDao.save(subject);
        SubjectResponse subjectResponse = subjectMapper.convertToResponse(saveSubject);
        return subjectResponse ;
    }

    @Override
    public void deleteMulti(List<Long> ids) throws ResourceNotFoundException {
        List<String> idList = new ArrayList<>();
        for(Long id : ids) {
            if(subjectDao.findById(id).isEmpty()) {
                idList.add(String.valueOf(id));
            }
        }
        if(!idList.isEmpty()) {
            throw new ResourceNotFoundException("Subject not found with id = " + String.join(",", idList));
        }
        subjectDao.deleteAllByIdInBatch(ids);
    }
}
