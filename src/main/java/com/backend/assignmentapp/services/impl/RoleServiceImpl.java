package com.backend.assignmentapp.services.impl;

import com.backend.assignmentapp.daos.RoleDao;
import com.backend.assignmentapp.dtos.requests.RoleRequest;
import com.backend.assignmentapp.dtos.responses.RoleResponse;
import com.backend.assignmentapp.entities.Role;
import com.backend.assignmentapp.exceptions.DuplicateException;
import com.backend.assignmentapp.exceptions.InvalidInputException;
import com.backend.assignmentapp.exceptions.NullReferenceException;
import com.backend.assignmentapp.exceptions.ResourceNotFoundException;
import com.backend.assignmentapp.modelmappers.RoleMapper;
import com.backend.assignmentapp.services.RoleService;
import com.backend.assignmentapp.validator.RoleValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class RoleServiceImpl implements RoleService {
    private final RoleDao roleDao;

    private final RoleMapper roleMapper;

    private final RoleValidator roleValidator;

    @Autowired
    public RoleServiceImpl(RoleDao roleDao, RoleMapper roleMapper, RoleValidator roleValidator) {
        this.roleDao = roleDao;
        this.roleMapper = roleMapper;
        this.roleValidator = roleValidator;
    }

    @Override
    public Page<RoleResponse> getAll(String name, Pageable paging) {
        Page<Role> pageRoles;
        if (name == null) {
            pageRoles = roleDao.findAll(paging.previousOrFirst());
        } else {
            pageRoles = roleDao.findByName(name, paging.previousOrFirst());
        }
        Page<RoleResponse> roleResponses = pageRoles.map(roleMapper::convertToResponse);
        return roleResponses;
    }

    @Override
    public RoleResponse get(Long id) throws ResourceNotFoundException {
        Optional<Role> roleOptional = roleDao.findById(id);
        if(roleOptional.isEmpty()) {
            throw new ResourceNotFoundException("Role not found with id = " + id);
        }
        Role role = roleOptional.get();
        RoleResponse roleResponse = roleMapper.convertToResponse(role);
        return roleResponse;
    }


    @Override
    public RoleResponse create(RoleRequest roleRequest) throws DuplicateException, InvalidInputException {
        roleRequest.setName(roleRequest.getName().replaceAll("\\s\\s+", " ").trim());
        if(roleDao.findDuplicateRoleWithoutId(roleRequest.getName()).isPresent()) {
            throw new DuplicateException("Role name already exists");
        }
        if(!roleValidator.checkFormatName(roleRequest.getName()).isEmpty()) {
            throw new InvalidInputException(roleValidator.checkFormatName(roleRequest.getName()));
        }
        Role role = roleMapper.convertToEntity(roleRequest);
        Role saveRole = roleDao.save(role);
        RoleResponse roleResponse = roleMapper.convertToResponse(saveRole);
        return roleResponse;
    }
    @Override
    @Transactional
    public RoleResponse update(Long id, RoleRequest roleRequest) throws ResourceNotFoundException, DuplicateException, NullReferenceException, InvalidInputException {
        roleRequest.setName(roleRequest.getName().replaceAll("\\s\\s+", " ").trim());
        Optional<Role> roleOptional = roleDao.findById(id);
        if(roleOptional.isEmpty()) {
            throw new ResourceNotFoundException("Role not found with id = " + id);
        }
        if(roleDao.findDuplicateRoleWithId(id, roleRequest.getName()).isPresent())  {
            throw new DuplicateException("Role name already exists");
        }
        if(!roleValidator.checkFormatName(roleRequest.getName()).isEmpty()) {
            throw new InvalidInputException(roleValidator.checkFormatName(roleRequest.getName()));
        }
        Role role = roleMapper.convertToEntity(roleRequest);
        role.setId(id);
        Role saveRole = roleDao.save(role);
        RoleResponse roleResponse = roleMapper.convertToResponse(saveRole);
        return roleResponse;
    }

    @Override
    public void deleteMulti(List<Long> ids) throws ResourceNotFoundException {
        List<String> idList = new ArrayList<>();
        for(Long id: ids) {
            if(roleDao.findById(id).isEmpty()) {
                idList.add(String.valueOf(id));
            }
        }
        if(!idList.isEmpty()) {
            String idString = String.join(",", idList);
            throw new ResourceNotFoundException("Role not found with id = " + idString);
        }
        roleDao.deleteAllByIdInBatch(ids);
    }
}
