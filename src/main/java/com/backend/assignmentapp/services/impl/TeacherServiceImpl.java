package com.backend.assignmentapp.services.impl;

import com.backend.assignmentapp.daos.TeacherDao;
import com.backend.assignmentapp.dtos.requests.*;
import com.backend.assignmentapp.dtos.responses.TeacherResponse;
import com.backend.assignmentapp.entities.Teacher;
import com.backend.assignmentapp.entities.User;
import com.backend.assignmentapp.exceptions.DuplicateException;
import com.backend.assignmentapp.exceptions.InvalidInputException;
import com.backend.assignmentapp.exceptions.NullReferenceException;
import com.backend.assignmentapp.exceptions.ResourceNotFoundException;
import com.backend.assignmentapp.modelmappers.TeacherMapper;
import com.backend.assignmentapp.modelmappers.UserMapper;
import com.backend.assignmentapp.services.TeacherService;
import com.backend.assignmentapp.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TeacherServiceImpl implements TeacherService {
    private final TeacherDao teacherDao;
    private final TeacherMapper teacherMapper;
    private final UserMapper userMapper;
    private final UserService userService;

    @Autowired
    public TeacherServiceImpl(TeacherDao teacherDao, TeacherMapper teacherMapper, UserMapper userMapper, UserService userService) {
        this.teacherDao = teacherDao;
        this.teacherMapper = teacherMapper;
        this.userMapper = userMapper;
        this.userService = userService;
    }

    @Override
    public Page<TeacherResponse> getAll(String name, Pageable pageable) {
        Page<Teacher> pageTeachers;
        if(name == null) {
            pageTeachers = teacherDao.findAll(pageable);
        } else {
            pageTeachers = teacherDao.findByName(name, pageable);
        }
        Page<TeacherResponse> teacherResponse = pageTeachers.map(teacherMapper::convertToResponse);
        return teacherResponse;
    }

    @Override
    public TeacherResponse create(TeacherUserRequest teacherUserRequest) throws InvalidInputException, DuplicateException, ResourceNotFoundException, NullReferenceException {
        TeacherRequest teacherRequest = convertToTeacherRequest(teacherUserRequest);
        Teacher teacher = teacherMapper.convertToEntity(teacherRequest);
        Teacher saveTeacher = teacherDao.save(teacher);
        TeacherResponse teacherResponse = teacherMapper.convertToResponse(saveTeacher);
        return teacherResponse;
    }

    @Override
    public TeacherResponse get(Long id) throws ResourceNotFoundException {
        Optional<Teacher> teacherOptional = teacherDao.findById(id);
        if (teacherOptional.isEmpty()) {
            throw new ResourceNotFoundException("Teacher not found with id = " + id);
        }
        Teacher teacher = teacherOptional.get();
        return teacherMapper.convertToResponse(teacher);
    }

    @Override
    public TeacherResponse update(Long id, TeacherUserRequest teacherUserRequest) throws InvalidInputException, DuplicateException, ResourceNotFoundException, NullReferenceException {
        if (teacherDao.findById(id).isEmpty()) {
            throw new ResourceNotFoundException("Teacher not found with id = " + id);
        }
        TeacherRequest teacherRequest = convertToTeacherRequest(id, teacherUserRequest);
        teacherRequest.setId(id);
        Teacher teacher = teacherMapper.convertToEntity(teacherRequest);
        Teacher saveTeacher = teacherDao.save(teacher);
        TeacherResponse teacherResponse = teacherMapper.convertToResponse(saveTeacher);
        return teacherResponse;
    }

    @Override
    public void deleteMulti(List<Long> ids) throws ResourceNotFoundException {
        List<String> idList = new ArrayList<>();
        List<Long> idUserList = new ArrayList<>();
        for (Long id : ids) {
            Optional<Teacher> teacherOptional = teacherDao.findById(id);
            if(teacherOptional.isEmpty()) {
                idList.add(String.valueOf(id));
            } else {
                idUserList.add(teacherOptional.get().getUser().getId());
            }
        }
        if(!idList.isEmpty()) {
            throw new ResourceNotFoundException("Teacher not found with id = " + String.join(",", idList));
        }
        teacherDao.deleteAllByIdInBatch(ids);
        if(!idUserList.isEmpty()) {
            userService.deleteMulti(idUserList);
        }
    }

    public TeacherRequest convertToTeacherRequest(TeacherUserRequest teacherUserRequest) throws InvalidInputException, DuplicateException, ResourceNotFoundException, NullReferenceException {
        TeacherRequest teacherRequest = new TeacherRequest();
        UserRequest userRequest = userMapper.convertToRequest(teacherUserRequest);
        User user = userService.createUser(userRequest);
        teacherRequest.setUser(user);
        return teacherRequest;
    }

    public TeacherRequest convertToTeacherRequest(Long id, TeacherUserRequest teacherUserRequest) throws InvalidInputException, DuplicateException, ResourceNotFoundException, NullReferenceException {
        TeacherRequest teacherRequest = new TeacherRequest();
        UserRequest userRequest = userMapper.convertToRequest(teacherUserRequest);
        userRequest.setId(id);
        User user = userService.updateUser(teacherDao.getById(id).getUser().getId(),userRequest);
        teacherRequest.setUser(user);
        return teacherRequest;
    }
}
