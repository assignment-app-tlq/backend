package com.backend.assignmentapp.services.impl;

import com.backend.assignmentapp.daos.MajorDao;
import com.backend.assignmentapp.dtos.responses.MajorResponse;
import com.backend.assignmentapp.entities.Major;
import com.backend.assignmentapp.exceptions.InvalidInputException;
import com.backend.assignmentapp.exceptions.ResourceNotFoundException;
import com.backend.assignmentapp.modelmappers.MajorMapper;
import com.backend.assignmentapp.dtos.requests.MajorRequest;
import com.backend.assignmentapp.exceptions.DuplicateException;
import com.backend.assignmentapp.services.MajorService;
import com.backend.assignmentapp.validator.MajorValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class MajorServiceImpl implements MajorService {
    private final MajorDao majorDao;

    private final MajorMapper majorMapper;

    private final MajorValidator majorValidator;

    @Autowired
    public MajorServiceImpl(MajorDao majorDao, MajorMapper majorMapper, MajorValidator majorValidator) {
        this.majorDao = majorDao;
        this.majorMapper = majorMapper;
        this.majorValidator = majorValidator;
    }

    @Override
    public Page<MajorResponse> getAll(String name, Pageable pageable) {
        Page<Major> majorPage;
        if(name == null) {
            majorPage = majorDao.findAll(pageable.previousOrFirst());
        } else {
            majorPage = majorDao.findByName(name, pageable.previousOrFirst());
        }
        Page<MajorResponse> majorResponsePage = majorPage.map(major -> majorMapper.convertToResponse(major));
        return majorResponsePage;
    }

    @Override
    public MajorResponse get(Long id) throws ResourceNotFoundException {
        Optional<Major> majorOptional = majorDao.findById(id);
        if(majorOptional.isEmpty()) {
            throw new ResourceNotFoundException("Major not found with id = " + id);
        }
        Major major = majorOptional.get();
        MajorResponse majorResponse = majorMapper.convertToResponse(major);
        return majorResponse;
    }

    @Override
    public MajorResponse create(MajorRequest majorRequest) throws DuplicateException, InvalidInputException {
        majorRequest.setName(majorRequest.getName().replaceAll("\\s\\s+", " ").trim());
        if(majorDao.findDuplicateMajorWithoutId(majorRequest.getName()).isPresent()) {
            throw new DuplicateException("Major name already exists");
        }
        if(!majorValidator.checkFormatName(majorRequest.getName()).isEmpty()) {
            throw new InvalidInputException(majorValidator.checkFormatName(majorRequest.getName()));
        }
        Major major = majorMapper.convertToEntity(majorRequest);
        Major saveMajor = majorDao.save(major);
        MajorResponse majorResponse = majorMapper.convertToResponse(saveMajor);
        return majorResponse;
    }

    @Override
    public MajorResponse update(Long id, MajorRequest majorRequest) throws ResourceNotFoundException, DuplicateException, InvalidInputException {
        majorRequest.setName(majorRequest.getName().replaceAll("\\s\\s+", " ").trim());
        Optional<Major> majorOptional = majorDao.findById(id);
        if(majorOptional.isEmpty()) {
            throw new ResourceNotFoundException("Major not found with id = " + id);
        }
        if(majorDao.findDuplicateMajorWithId(id, majorRequest.getName()).isPresent()) {
            throw new DuplicateException("Major already exists");
        }
        if(!majorValidator.checkFormatName(majorRequest.getName()).isEmpty()) {
            throw new InvalidInputException(majorValidator.checkFormatName(majorRequest.getName()));
        }
        Major major = majorMapper.convertToEntity(majorRequest);
        major.setId(id);
        Major saveMajor = majorDao.save(major);
        MajorResponse majorResponse = majorMapper.convertToResponse(saveMajor);
        return majorResponse;
    }

    @Override
    public void deleteMulti(List<Long> ids) throws ResourceNotFoundException {
        List<String> idList = new ArrayList<String>();
        for(Long id: ids) {
            if(majorDao.findById(id).isEmpty()) {
                idList.add(String.valueOf(id));
            }
        }
        if(!idList.isEmpty()) {
            throw new ResourceNotFoundException("Major not found with id = " + String.join(",", idList));
        }
        majorDao.deleteAllByIdInBatch(ids);
    }
}
