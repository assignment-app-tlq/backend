package com.backend.assignmentapp.services.impl;

import com.backend.assignmentapp.daos.CourseDao;
import com.backend.assignmentapp.dtos.requests.CourseRequest;
import com.backend.assignmentapp.dtos.responses.CourseResponse;
import com.backend.assignmentapp.entities.Course;
import com.backend.assignmentapp.exceptions.DuplicateException;
import com.backend.assignmentapp.exceptions.InvalidInputException;
import com.backend.assignmentapp.exceptions.NullReferenceException;
import com.backend.assignmentapp.exceptions.ResourceNotFoundException;
import com.backend.assignmentapp.modelmappers.CourseMapper;
import com.backend.assignmentapp.services.CourseService;
import com.backend.assignmentapp.validator.CourseValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CourseServiceImpl implements CourseService {
    private final CourseDao courseDao;

    private final CourseMapper courseMapper;

    private final CourseValidator courseValidator;

    @Autowired
    public CourseServiceImpl(CourseDao courseDao, CourseMapper courseMapper, CourseValidator courseValidator) {
        this.courseDao = courseDao;
        this.courseMapper = courseMapper;
        this.courseValidator = courseValidator;
    }

    @Override
    public Page<CourseResponse> getAll(String name, Pageable pageable) {
        Page<Course> pageCourses;
        if (name == null) {
            pageCourses = courseDao.findAll(pageable.previousOrFirst());
        } else {
            pageCourses = courseDao.findByName(name, pageable.previousOrFirst());
        }
        Page<CourseResponse> courseResponse = pageCourses.map(course -> courseMapper.convertToResponse(course));
        return courseResponse;
    }

    @Override
    public CourseResponse get(Long id) throws ResourceNotFoundException {
        Optional<Course> courseOptional = courseDao.findById(id);
        if(courseOptional.isEmpty()) {
            throw new ResourceNotFoundException("Course not found with id = " + id);
        }
        Course course = courseOptional.get();
        CourseResponse courseResponse = courseMapper.convertToResponse(course);
        return courseResponse;
    }

    @Override
    public CourseResponse update(Long id, CourseRequest courseRequest) throws DuplicateException, InvalidInputException, ResourceNotFoundException {
        courseRequest.setName(courseRequest.getName().replaceAll("\\s\\s+", " ").trim());
        Optional<Course> courseOptional = courseDao.findById(id);
        if(courseOptional.isEmpty()) {
            throw new ResourceNotFoundException("Course not found with id = " + id);
        }
        courseRequest.setName(courseRequest.getName().trim());
        if(courseDao.findDuplicateCourseWithId(id, courseRequest.getName()).isPresent()) {
            throw new DuplicateException("Course name already exists");
        }
        if(!courseValidator.checkFormatName(courseRequest.getName()).isEmpty()) {
            throw new InvalidInputException(courseValidator.checkFormatName(courseRequest.getName()));
        }
        Course course = courseMapper.convertToEntity(courseRequest);
        course.setId(id);
        Course saveCourse = courseDao.save(course);
        CourseResponse courseResponse = courseMapper.convertToResponse(saveCourse);
        return courseResponse;
    }


    @Override
    public CourseResponse create(CourseRequest courseRequest) throws DuplicateException, InvalidInputException {
        courseRequest.setName(courseRequest.getName().replaceAll("\\s\\s+", " ").trim());
        if(courseDao.findDuplicateCourseWithoutId(courseRequest.getName()).isPresent()) {
            throw new DuplicateException("Course name already exists");
        }
        if(!courseValidator.checkFormatName(courseRequest.getName()).isEmpty()) {
            throw new InvalidInputException(courseValidator.checkFormatName(courseRequest.getName()));
        }
        Course course = courseMapper.convertToEntity(courseRequest);
        Course saveCourse = courseDao.save(course);
        CourseResponse courseResponse = courseMapper.convertToResponse(saveCourse);
        return courseResponse;
    }

    @Override
    public void deleteMulti(List<Long> ids) throws ResourceNotFoundException {
        List<String> idList = new ArrayList<>();
        for(Long id : ids) {
            if(courseDao.findById(id).isEmpty()) {
                idList.add(String.valueOf(id));
            }
        }
        if(!idList.isEmpty()) {
            String idString = String.join(",", idList);
            throw new ResourceNotFoundException("Course not found with id = " + idString);
        }
        courseDao.deleteAllByIdInBatch(ids);
    }

}
