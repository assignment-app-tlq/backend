package com.backend.assignmentapp.services.impl;

import com.backend.assignmentapp.daos.ClassDao;
import com.backend.assignmentapp.daos.CourseDao;
import com.backend.assignmentapp.daos.MajorDao;
import com.backend.assignmentapp.dtos.requests.ClassRequest;
import com.backend.assignmentapp.dtos.responses.ClassResponse;
import com.backend.assignmentapp.entities.Class;
import com.backend.assignmentapp.exceptions.DuplicateException;
import com.backend.assignmentapp.exceptions.InvalidInputException;
import com.backend.assignmentapp.exceptions.NullReferenceException;
import com.backend.assignmentapp.exceptions.ResourceNotFoundException;
import com.backend.assignmentapp.modelmappers.ClassMapper;
import com.backend.assignmentapp.services.ClassService;
import com.backend.assignmentapp.validator.ClassValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ClassServiceImpl implements ClassService {
    private final ClassDao classDao;

    private final CourseDao courseDao;

    private final MajorDao majorDao;

    private final ClassMapper classMapper;

    private final ClassValidator classValidator;

    @Autowired
    public ClassServiceImpl(ClassDao classDao, CourseDao courseDao, MajorDao majorDao, ClassMapper classMapper, ClassValidator classValidator) {
        this.classDao = classDao;
        this.courseDao = courseDao;
        this.majorDao = majorDao;
        this.classMapper = classMapper;
        this.classValidator = classValidator;
    }

    @Override
    public Page<ClassResponse> getAll(String name, Pageable pageable) {
        Page<Class> pageClasses;
        if(name == null) {
            pageClasses = classDao.findAll(pageable.previousOrFirst());
        } else {
            pageClasses = classDao.findByName(name, pageable.previousOrFirst());
        }
        Page<ClassResponse> classResponse = pageClasses.map(classes -> classMapper.convertToResponse(classes));
        return classResponse;
    }

    @Override
    public ClassResponse create(ClassRequest classRequest) throws ResourceNotFoundException, InvalidInputException, DuplicateException, NullReferenceException {
        classRequest.setName(classRequest.getName().replaceAll("\\s\\s+", " ").trim());
        if(classRequest.getCourseId() == null && classRequest.getMajorId() == null) {
            throw new NullReferenceException("Course ID and Major ID cannot be null");
        } else if (classRequest.getCourseId() == null) {
            throw new NullReferenceException("Course ID cannot be null");
        } else if (classRequest.getMajorId() == null){
            throw new NullReferenceException("Major ID cannot be null");
        }
        classRequest.setName(classRequest.getName().trim());
        if(!majorDao.existsById(classRequest.getMajorId())) {
            throw new ResourceNotFoundException("Major not found with id = " + classRequest.getMajorId());
        }
        if(!courseDao.existsById(classRequest.getCourseId())) {
            throw new ResourceNotFoundException("Course not found with id = " + classRequest.getCourseId());
        }
        if(classDao.findDuplicateClassWithoutId(classRequest.getName(), classRequest.getMajorId(), classRequest.getCourseId()).isPresent()) {
            throw new DuplicateException("Class " + classRequest.getName() + " already exists");
        }
        if(!classValidator.checkFormatName(classRequest.getName()).isEmpty()) {
            throw new InvalidInputException(classValidator.checkFormatName(classRequest.getName()));
        }
        Class classes = classMapper.convertToEntity(classRequest);
        Class saveClass = classDao.save(classes);
        ClassResponse classResponse = classMapper.convertToResponse(saveClass);
        return classResponse;
    }

    @Override
    public ClassResponse get(Long id) throws ResourceNotFoundException {
        Optional<Class> classOptional = classDao.findById(id);
        if(classOptional.isEmpty()) {
            throw new ResourceNotFoundException("Class not found with id = " + id);
        }
        Class classes = classOptional.get();
        ClassResponse classResponse = classMapper.convertToResponse(classes);
        return classResponse;
    }

    @Override
    public ClassResponse update(Long id, ClassRequest classRequest) throws ResourceNotFoundException, DuplicateException, InvalidInputException, NullReferenceException {
        classRequest.setName(classRequest.getName().replaceAll("\\s\\s+", " ").trim());
        if(classRequest.getCourseId() == null && classRequest.getMajorId() == null) {
            throw new NullReferenceException("Course and Major cannot be null");
        } else if (classRequest.getCourseId() == null) {
            throw new NullReferenceException("Course cannot be null");
        } else if (classRequest.getMajorId() == null){
            throw new NullReferenceException("Major cannot be null");
        }
        classRequest.setName(classRequest.getName().trim());
        Optional<Class> classOptional = classDao.findById(id);
        if(classOptional.isEmpty()) {
            throw new ResourceNotFoundException("Class not found with id = " + id);
        }
        if(!majorDao.existsById(classRequest.getMajorId())) {
            throw new ResourceNotFoundException("Major not found with id = " + classRequest.getMajorId());
        }
        if(!courseDao.existsById(classRequest.getCourseId())) {
            throw new ResourceNotFoundException("Course not found with id = " + classRequest.getCourseId());
        }
        if(classDao.findDuplicateClassWithId(id, classRequest.getName(), classRequest.getMajorId(), classRequest.getCourseId()).isPresent()) {
            throw new DuplicateException("Class " + classRequest.getName() + " already exists");
        }
        if(!classValidator.checkFormatName(classRequest.getName()).isEmpty()) {
            throw new InvalidInputException(classValidator.checkFormatName(classRequest.getName()));
        }
        Class classes = classMapper.convertToEntity(classRequest);
        classes.setId(id);
        Class saveClass = classDao.save(classes);
        ClassResponse classResponse = classMapper.convertToResponse(saveClass);
        return classResponse;
    }

    @Override
    public void deleteMulti(List<Long> ids) throws ResourceNotFoundException {
        List<String> idList = new ArrayList<>();
        for(Long id : ids) {
            if(classDao.findById(id).isEmpty()) {
                idList.add(String.valueOf(id));
            }
        }
        if(!idList.isEmpty()) {
            throw new ResourceNotFoundException("Class not found with id = " + String.join(",", idList));
        }
        classDao.deleteAllByIdInBatch(ids);
    }


}
