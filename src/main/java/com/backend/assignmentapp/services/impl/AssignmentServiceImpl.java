package com.backend.assignmentapp.services.impl;

import com.backend.assignmentapp.daos.AssignmentDao;
import com.backend.assignmentapp.daos.DivisionDao;
import com.backend.assignmentapp.dtos.requests.AssignmentRequest;
import com.backend.assignmentapp.dtos.responses.AssignmentResponse;
import com.backend.assignmentapp.entities.Assignment;
import com.backend.assignmentapp.exceptions.ResourceNotFoundException;
import com.backend.assignmentapp.modelmappers.AssignmentMapper;
import com.backend.assignmentapp.services.AssignmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;

@Service
public class AssignmentServiceImpl implements AssignmentService {
    private final AssignmentDao assignmentDao;
    private final AssignmentMapper assignmentMapper;
    private final DivisionDao divisionDao;

    @Value("${file.upload-dir}")
    String FILE_DIRECTORY;

    @Autowired
    public AssignmentServiceImpl(AssignmentDao assignmentDao, AssignmentMapper assignmentMapper, DivisionDao divisionDao) {
        this.assignmentDao = assignmentDao;
        this.assignmentMapper = assignmentMapper;
        this.divisionDao = divisionDao;
    }

    @Override
    public Page<AssignmentResponse> getAll(String name, Pageable pageable) {
        Page<Assignment> pageAssignments;
        if(name == null) {
            pageAssignments = assignmentDao.findAll(pageable);
        } else {
            pageAssignments = assignmentDao.findAll(name, pageable);
        }
        Page<AssignmentResponse> assignmentResponsePage = pageAssignments.map(assignmentMapper::convertToResponse);
        return assignmentResponsePage;
    }

    @Override
    public AssignmentResponse get(Long id) {
        return null;
    }

    @Override
    public AssignmentResponse create(AssignmentRequest assignmentRequest) throws IOException, ResourceNotFoundException {
        assignmentRequest.setName(assignmentRequest.getName().replaceAll("\\s\\s+", " ").trim());
        MultipartFile file = assignmentRequest.getQuestionFile();
//        byte[] data = file.getBytes();
        Path path = Paths.get(FILE_DIRECTORY + file.getOriginalFilename());
//        if(path.is)
//        for(File f : Objects.requireNonNull(path.toFile().listFiles())) {
//
//        }
//        Files.write(path, data);
        Files.copy(file.getInputStream(), path);
        if(divisionDao.findById(assignmentRequest.getDivisionId()).isEmpty()) {
            throw new ResourceNotFoundException("Division not found with id = " + assignmentRequest.getDivisionId());
        }
        Assignment assignment = assignmentMapper.convertToEntity(assignmentRequest);
        Assignment saveAssignment = assignmentDao.save(assignment);
        return assignmentMapper.convertToResponse(saveAssignment);
    }

    @Override
    public AssignmentResponse update(Long id, AssignmentRequest assignmentRequest) {
        return null;
    }

    @Override
    public void deleteMulti(List<Long> ids) {
//        assignmentDao.deleteAllByIdInBatch(ids);
        assignmentDao.deleteAll();
    }
}
