package com.backend.assignmentapp.services.impl;

import com.backend.assignmentapp.daos.ClassDao;
import com.backend.assignmentapp.daos.StudentDao;
import com.backend.assignmentapp.dtos.requests.StudentRequest;
import com.backend.assignmentapp.dtos.requests.StudentUserRequest;
import com.backend.assignmentapp.dtos.requests.UserRequest;
import com.backend.assignmentapp.dtos.responses.StudentResponse;
import com.backend.assignmentapp.entities.Student;
import com.backend.assignmentapp.entities.User;
import com.backend.assignmentapp.exceptions.DuplicateException;
import com.backend.assignmentapp.exceptions.InvalidInputException;
import com.backend.assignmentapp.exceptions.NullReferenceException;
import com.backend.assignmentapp.exceptions.ResourceNotFoundException;
import com.backend.assignmentapp.modelmappers.StudentMapper;
import com.backend.assignmentapp.modelmappers.UserMapper;
import com.backend.assignmentapp.services.StudentService;
import com.backend.assignmentapp.services.UserService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class StudentServiceImpl implements StudentService {
    private final StudentMapper studentMapper;
    private final StudentDao studentDao;
    private final UserService userService;
    private final ClassDao classDao;
    private final UserMapper userMapper;

    public StudentServiceImpl(StudentMapper studentMapper, StudentDao studentDao, UserService userService, ClassDao classDao, UserMapper userMapper) {
        this.studentMapper = studentMapper;
        this.studentDao = studentDao;
        this.userService = userService;
        this.classDao = classDao;
        this.userMapper = userMapper;
    }

    @Override
    public Page<StudentResponse> getAll(String name, String className, Pageable pageable) {
        Page<Student> pageStudents;
        if(name != null && className != null) {
            pageStudents = studentDao.findAll(name, className, pageable);
        }
        if(name == null && className != null) {
            pageStudents = studentDao.findAll(className, pageable);
        }
        if(name != null && className == null) {
            pageStudents = studentDao.findByStudentName(name, pageable);
        }
        else {
            pageStudents = studentDao.findAll(pageable);
        }
        Page<StudentResponse> studentResponsePage = pageStudents.map(studentMapper::convertToResponse);
        return studentResponsePage;
    }

    @Override
    public StudentResponse get(Long id) throws ResourceNotFoundException {
        Optional<Student> studentOptional = studentDao.findById(id);
        if(studentOptional.isEmpty()) {
            throw new ResourceNotFoundException("Student not found with id = " + id);
        }
        Student student = studentOptional.get();
        StudentResponse studentResponse = studentMapper.convertToResponse(student);
        return studentResponse;
    }

    @Override
    public StudentResponse save(StudentUserRequest studentUserRequest) throws InvalidInputException, DuplicateException, ResourceNotFoundException, NullReferenceException {
        StudentRequest studentRequest = convertToStudentRequest(studentUserRequest);
        Student student = studentMapper.convertToEntity(studentRequest);
        Student saveStudent = studentDao.save(student);
        StudentResponse studentResponse = studentMapper.convertToResponse(saveStudent);
        return studentResponse;
    }

    @Override
    public StudentResponse update(Long id, StudentUserRequest studentUserRequest) throws InvalidInputException, DuplicateException, ResourceNotFoundException, NullReferenceException {
        if(studentDao.findById(id).isEmpty()) {
            throw new ResourceNotFoundException("Student not found with id = " + id);
        }
        StudentRequest studentRequest = convertToStudentRequest(id, studentUserRequest);
        studentRequest.setId(id);
        Student student = studentMapper.convertToEntity(studentRequest);
        Student saveStudent = studentDao.save(student);
        StudentResponse studentResponse = studentMapper.convertToResponse(saveStudent);
        return studentResponse;
    }

    @Override
    public void deleteMulti(List<Long> ids) throws ResourceNotFoundException {
        List<String> idList = new ArrayList<>();
        List<Long> idUserList = new ArrayList<>();
        for(Long id : ids) {
            if(studentDao.findById(id).isEmpty()) {
                idList.add(String.valueOf(id));
            } else {
                idUserList.add(studentDao.getById(id).getUser().getId());
            }
        }
        if(!idList.isEmpty()) {
            throw new ResourceNotFoundException("Student not found with id = " + String.join(",", idList));
        }
        studentDao.deleteAllByIdInBatch(ids);
        if(!idUserList.isEmpty()) {
            userService.deleteMulti(idUserList);
        }
    }

    public StudentRequest convertToStudentRequest(StudentUserRequest studentUserRequest) throws InvalidInputException, DuplicateException, ResourceNotFoundException, NullReferenceException {
        if(classDao.findById(studentUserRequest.getClassId()).isEmpty()) {
            throw new ResourceNotFoundException("Class not found with id = " + studentUserRequest.getClassId());
        }
        StudentRequest studentRequest = new StudentRequest();
        UserRequest userRequest = userMapper.convertToRequest(studentUserRequest);
        User user = userService.createUser(userRequest);
        studentRequest.setUser(user);
        studentRequest.setClassId(studentUserRequest.getClassId());
        return studentRequest;
    }

    public StudentRequest convertToStudentRequest(Long id, StudentUserRequest studentUserRequest) throws ResourceNotFoundException, InvalidInputException, DuplicateException, NullReferenceException {
        if(classDao.findById(studentUserRequest.getClassId()).isEmpty()) {
            throw new ResourceNotFoundException("Class not found with id = " + studentUserRequest.getClassId());
        }
        StudentRequest studentRequest = new StudentRequest();
        UserRequest userRequest = userMapper.convertToRequest(studentUserRequest);
        User user = userService.updateUser(studentDao.getById(id).getUser().getId(),userRequest);
        studentRequest.setUser(user);
        studentRequest.setClassId(studentUserRequest.getClassId());
        return studentRequest;
    }
}
