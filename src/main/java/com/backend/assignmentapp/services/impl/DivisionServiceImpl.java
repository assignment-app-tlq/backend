package com.backend.assignmentapp.services.impl;

import com.backend.assignmentapp.daos.ClassDao;
import com.backend.assignmentapp.daos.DivisionDao;
import com.backend.assignmentapp.daos.SubjectDao;
import com.backend.assignmentapp.daos.TeacherDao;
import com.backend.assignmentapp.dtos.requests.DivisionRequest;
import com.backend.assignmentapp.dtos.responses.DivisionResponse;
import com.backend.assignmentapp.entities.Division;
import com.backend.assignmentapp.exceptions.DuplicateException;
import com.backend.assignmentapp.exceptions.ResourceNotFoundException;
import com.backend.assignmentapp.modelmappers.DivisionMapper;
import com.backend.assignmentapp.services.DivisionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class DivisionServiceImpl implements DivisionService {
    private final DivisionDao divisionDao;
    private final DivisionMapper divisionMapper;
    private final ClassDao classDao;
    private final TeacherDao teacherDao;
    private final SubjectDao subjectDao;

    @Autowired
    public DivisionServiceImpl(DivisionDao divisionDao, DivisionMapper divisionMapper, ClassDao classDao, TeacherDao teacherDao, SubjectDao subjectDao) {
        this.divisionDao = divisionDao;
        this.divisionMapper = divisionMapper;
        this.classDao = classDao;
        this.teacherDao = teacherDao;
        this.subjectDao = subjectDao;
    }

    @Override
    public Page<DivisionResponse> getAll(String subjectName, String className, String teacherName, Pageable pageable) {
        Page<Division> pageDivisions;
        if(subjectName == null && className == null && teacherName == null) {
            pageDivisions = divisionDao.findAll(pageable);
        } else {
            pageDivisions = divisionDao.findAll(subjectName, className, teacherName, pageable);
        }
        Page<DivisionResponse> divisionResponse = pageDivisions.map(divisionMapper::convertToResponse);
        return divisionResponse;
    }

    @Override
    public DivisionResponse get(Long id) throws ResourceNotFoundException {
        Optional<Division> divisionOptional = divisionDao.findById(id);
        if(divisionOptional.isEmpty()) {
            throw new ResourceNotFoundException("Division not found with id = " + id);
        }
        DivisionResponse divisionResponse = divisionMapper.convertToResponse(divisionOptional.get());
        return divisionResponse;
    }

    @Override
    public DivisionResponse create(DivisionRequest divisionRequest) throws ResourceNotFoundException, DuplicateException {
        if(classDao.findById(divisionRequest.getClassId()).isEmpty()) {
            throw new ResourceNotFoundException("Class not found with id = " + divisionRequest.getClassId());
        }
        if(subjectDao.findById(divisionRequest.getSubjectId()).isEmpty()) {
            throw new ResourceNotFoundException("Subject not found with id = " + divisionRequest.getSubjectId());
        }
        if(teacherDao.findById(divisionRequest.getTeacherId()).isEmpty()) {
            throw new ResourceNotFoundException("Teacher not found with id = " + divisionRequest.getTeacherId());
        }
        if(divisionDao.findDuplicateRoleWithoutId(divisionRequest.getSubjectId(), divisionRequest.getClassId(), divisionRequest.getTeacherId()).isPresent()) {
            throw new DuplicateException("Division already exists");
        }
        Division division = divisionMapper.convertToEntity(divisionRequest);
        Division saveDivision = divisionDao.save(division);
        DivisionResponse divisionResponse = divisionMapper.convertToResponse(saveDivision);
        return divisionResponse;
    }

    @Override
    public void deleteMulti(List<Long> ids) throws ResourceNotFoundException {
        List<String> idList = new ArrayList<>();
        for(Long id: ids) {
            if(divisionDao.findById(id).isEmpty()) {
                idList.add(String.valueOf(id));
            }
        }
        if(!idList.isEmpty()) {
            throw new ResourceNotFoundException("Division not found with id = " + String.join(",", idList));
        }
        divisionDao.deleteAllByIdInBatch(ids);
    }

    @Override
    public DivisionResponse update(Long id, DivisionRequest divisionRequest) throws ResourceNotFoundException, DuplicateException {
        if(divisionDao.findById(divisionRequest.getId()).isEmpty()) {
            throw new ResourceNotFoundException("Division not found with id = " + id);
        }
        if(classDao.findById(divisionRequest.getClassId()).isEmpty()) {
            throw new ResourceNotFoundException("Class not found with id = " + divisionRequest.getClassId());
        }
        if(subjectDao.findById(divisionRequest.getSubjectId()).isEmpty()) {
            throw new ResourceNotFoundException("Subject not found with id = " + divisionRequest.getSubjectId());
        }
        if(teacherDao.findById(divisionRequest.getTeacherId()).isEmpty()) {
            throw new ResourceNotFoundException("Teacher not found with id = " + divisionRequest.getTeacherId());
        }
        if(divisionDao.findDuplicateRoleWithId(divisionRequest.getId(), divisionRequest.getSubjectId(), divisionRequest.getClassId(), divisionRequest.getTeacherId()).isPresent()) {
            throw new DuplicateException("Division already exists");
        }
        Division division = divisionMapper.convertToEntity(divisionRequest);
        division.setId(id);
        Division saveDivision = divisionDao.save(division);
        DivisionResponse divisionResponse = divisionMapper.convertToResponse(saveDivision);
        return divisionResponse;
    }
}
