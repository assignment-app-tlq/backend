package com.backend.assignmentapp.services;

import com.backend.assignmentapp.dtos.requests.AssignmentRequest;
import com.backend.assignmentapp.dtos.responses.AssignmentResponse;
import com.backend.assignmentapp.exceptions.ResourceNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

@Component
public interface AssignmentService {

    Page<AssignmentResponse> getAll(String name, Pageable pageable);

    AssignmentResponse get(Long id);

    AssignmentResponse create(AssignmentRequest assignmentRequest) throws IOException, ResourceNotFoundException;

    AssignmentResponse update(Long id, AssignmentRequest assignmentRequest);

    void deleteMulti(List<Long> ids);
}
