package com.backend.assignmentapp.validator;

import com.backend.assignmentapp.models.CustomError;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ClassValidator {
    public List<CustomError> checkFormatName(String input) {
        List<CustomError> result = new ArrayList<>();
        if(input.isEmpty()) {
            result.add(new CustomError("Name cannot be empty"));
        }
        else {
            if (!input.matches("^([A-Za-z0-9\\S]*)$")) {
                result.add(new CustomError("Name cannot contain the whitespace"));
            }
            if (input.length() > 40) {
                result.add(new CustomError("Name length cannot greater than 40"));
            }
        }
        return result;
    }
}
