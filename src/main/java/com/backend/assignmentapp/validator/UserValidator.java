package com.backend.assignmentapp.validator;

import com.backend.assignmentapp.models.CustomError;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserValidator {
    public List<CustomError> checkFormatName(String input) {
        List<CustomError> result = new ArrayList<>();
        if(input.isEmpty()) {
            result.add(new CustomError("Name cannot be empty"));
        }
        else {
            if (input.matches("^.*\\d+.*")) {
                result.add(new CustomError("Name cannot contain numbers"));
            }
            if (!input.matches("^[aAàÀảẢãÃáÁạẠăĂằẰẳẲẵẴắẮặẶâÂầẦẩẨẫẪấẤậẬbBcCdDđĐeEèÈẻẺẽẼéÉẹẸêÊềỀểỂễỄếẾệỆ\n" +
                    "fFgGhHiIìÌỉỈĩĨíÍịỊjJkKlLmMnNoOòÒỏỎõÕóÓọỌôÔồỒổỔỗỖốỐộỘơƠờỜởỞỡỠớỚợỢpPqQrRsStTu\n" +
                    "UùÙủỦũŨúÚụỤưƯừỪửỬữỮứỨựỰvVwWxXyYỳỲỷỶỹỸýÝỵỴzZ\\s]*$")) {
                result.add(new CustomError("Name is not correct"));
            }
            if (input.length() > 60) {
                result.add(new CustomError("Name length cannot greater than 60"));
            }
        }
        return result;
    }

    public CustomError checkFormatEmailAddress(String email) {
        CustomError customError = null;
        if(!email.matches("^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}")) {
            customError = new CustomError("Email is not correct");
        }
        return customError;
    }

    public List<CustomError> checkFormatPassword(String password) {
        List<CustomError> result = new ArrayList<>();
        if (password.length() > 15 || password.length() < 8)
        {
            result.add(new CustomError("Password must be less than 20 and more than 8 characters in length."));
        }
        String upperCaseChars = "(.*[A-Z].*)";
        if (!password.matches(upperCaseChars ))
        {
            result.add(new CustomError("Password must have atleast one uppercase character"));
        }
        String lowerCaseChars = "(.*[a-z].*)";
        if (!password.matches(lowerCaseChars ))
        {
            result.add(new CustomError("Password must have atleast one lowercase character"));
        }
        String numbers = "(.*[0-9].*)";
        if (!password.matches(numbers ))
        {
            result.add(new CustomError("Password must have atleast one number"));
        }
        String specialChars = "(.*[@,#,$,%].*$)";
        if (!password.matches(specialChars ))
        {
            result.add(new CustomError("Password must have atleast one special character among @#$%"));
        }
        return result;
    }
}
